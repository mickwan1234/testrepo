/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nghia.DTO.FoodDTO;
import nghia.beans.Beans;
import nghia.errorObject.ErrorObject;

/**
 *
 * @author mickw
 */
public class EditController extends HttpServlet {

    private static final String INVALID = "update.jsp";
    private static final String SUCCESS = "SearchController";
    private static final String ERROR = "error.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        ErrorObject errorObj = new ErrorObject();
        boolean valid = true;
        HttpSession session = request.getSession();
        try {
            String foodID = request.getParameter("txtFoodID");
            String foodName = request.getParameter("txtFoodName");
            float price = Float.parseFloat(request.getParameter("txtPrice"));
            String description = request.getParameter("txtDescription");
            String type = request.getParameter("txtType");
            String status = request.getParameter("cblStatus");
            FoodDTO dto = new FoodDTO(foodID, foodName, description, type, status, price);
            if (foodID.length() == 0) {
                errorObj.setIdError("Id must not be blank");
                valid = false;
            }
            if (foodName.length() == 0) {
                errorObj.setIdError("food name must not be blank");
                valid = false;
            }
            if (price <= 0) {
                errorObj.setPriceError("Price must not be smaller than 0");
                valid = false;
            }
            if (description.length() == 0) {
                errorObj.setDescriptionError("Description must not be blank");
                valid = false;
            }
            if (type.length() == 0) {
                errorObj.setTypeError("type must not be blank");
                valid = false;
            }
            if (valid) {
                Beans beans = new Beans();
                beans.setDto(dto);
                if (beans.update()) {
                    session.setAttribute("INVALID", errorObj);
                    url = SUCCESS;
                } else {
                    request.setAttribute("ERROR", "Update failed");
                }
            } else {
                session.setAttribute("INVALID", errorObj);
                request.setAttribute("INFO", dto);
                url = INVALID;
            }
        } catch (Exception e) {
            log("Error at EditController: " + e.getMessage());
            if (e.getMessage().contains("NumberFormat")) {
                errorObj.setPriceError("Not a number");
                valid = false;
                url = INVALID;
            }
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
