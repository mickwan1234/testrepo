/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.errorObject;

import java.io.Serializable;

/**
 *
 * @author mickw
 */
public class ErrorObject implements Serializable {

    String rangeError, idError, nameError, priceError, descriptionError, typeError, statusError;

    public void setIdError(String idError) {
        this.idError = idError;
    }

    public void setNameError(String nameError) {
        this.nameError = nameError;
    }

    public void setPriceError(String priceError) {
        this.priceError = priceError;
    }

    public void setDescriptionError(String descriptionError) {
        this.descriptionError = descriptionError;
    }

    public void setTypeError(String typeError) {
        this.typeError = typeError;
    }

    public void setStatusError(String statusError) {
        this.statusError = statusError;
    }

    public String getIdError() {
        return idError;
    }

    public String getNameError() {
        return nameError;
    }

    public String getPriceError() {
        return priceError;
    }

    public String getDescriptionError() {
        return descriptionError;
    }

    public String getTypeError() {
        return typeError;
    }

    public String getStatusError() {
        return statusError;
    }

    public ErrorObject() {
    }

    public String getRangeError() {
        return rangeError;
    }

    public void setRangeError(String rangeError) {
        this.rangeError = rangeError;
    }

}
