/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.beans;

import java.io.Serializable;
import java.util.List;
import nghia.DTO.FoodDTO;
import nghia.daos.FoodDAO;

/**
 *
 * @author mickw
 */
public class Beans implements Serializable {

    float lowerRange, upperRange;
    String foodID;
    FoodDTO dto;

    public void setDto(FoodDTO dto) {
        this.dto = dto;
    }

    public FoodDTO getDto() {
        return dto;
    }

    public void setFoodID(String foodID) {
        this.foodID = foodID;
    }

    public String getFoodID() {
        return foodID;
    }

    public Beans() {
    }

    public float getLowerRange() {
        return lowerRange;
    }

    public float getUpperRange() {
        return upperRange;
    }

    public void setLowerRange(float lowerRange) {
        this.lowerRange = lowerRange;
    }

    public void setUpperRange(float upperRange) {
        this.upperRange = upperRange;
    }

    public List<FoodDTO> findByRange() throws Exception {
        FoodDAO dao = new FoodDAO();
        return dao.findByRange(upperRange, lowerRange);
    }

    public boolean delete() throws Exception {
        FoodDAO dao = new FoodDAO();
        return dao.delete(foodID);
    }

    public FoodDTO findByPrimaryKey() throws Exception {
        FoodDAO dao = new FoodDAO();
        return dao.findByPrimaryKey(foodID);
    }

    public boolean update() throws Exception {
        FoodDAO dao = new FoodDAO();
        return dao.update(dto);
    }
    public boolean insert() throws Exception{
        FoodDAO dao = new FoodDAO();
        return dao.insert(dto);
    }
}
