/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.DTO;

import java.io.Serializable;

/**
 *
 * @author mickw
 */
public class FoodDTO implements Serializable{
    String foodID, foodName, description, type, status;
    float price;

    public FoodDTO(String foodID, String foodName, String description, String type, String status, float price) {
        this.foodID = foodID;
        this.foodName = foodName;
        this.description = description;
        this.type = type;
        this.status = status;
        this.price = price;
    }

    public String getFoodName() {
        return foodName;
    }

    public void setFoodName(String foodName) {
        this.foodName = foodName;
    }

    public FoodDTO() {
    }

    public void setFoodID(String foodID) {
        this.foodID = foodID;
    }

    public void setfoodName(String foodName) {
        this.foodName = foodName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getFoodID() {
        return foodID;
    }

    public String getfoodName() {
        return foodName;
    }

    public String getDescription() {
        return description;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public float getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "FoodDTO{" + "foodID=" + foodID + ", foodName=" + foodName + ", description=" + description + ", type=" + type + ", status=" + status + ", price=" + price + '}';
    }
    
}
