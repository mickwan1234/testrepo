/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import nghia.DTO.FoodDTO;
import nghia.connection.MyConnection;

/**
 *
 * @author mickw
 */
public class FoodDAO implements Serializable {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public FoodDAO() {
    }

    private void closeConnection() throws Exception {
        if (conn != null) {
            conn.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public List<FoodDTO> findByRange(float upperRange, float lowerRange) throws Exception {
        List<FoodDTO> dtoList = new ArrayList<>();
        String sql = "Select * from tbl_Food where Price Between ? and ?";
        System.out.println(upperRange);
        System.out.println(lowerRange);
        try {
            conn = MyConnection.getMyConection();
            ps = conn.prepareStatement(sql);
            ps.setFloat(1, lowerRange);
            ps.setFloat(2, upperRange);
            System.out.println("success");
            rs = ps.executeQuery();
            while (rs.next()) {
                String foodID = rs.getString("FoodID");
                String foodName = rs.getString("FoodName");
                float price = rs.getFloat("Price");
                String description = rs.getString("Description");
                String type = rs.getString("Type");
                String status = rs.getString("Status");
                FoodDTO dto = new FoodDTO(foodID, foodName, description, type, status, price);
                dtoList.add(dto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return dtoList;
    }

    public boolean delete(String id) throws Exception {

        boolean check = false;
        try {
            String sql = "Delete from tbl_Food where FoodID = ?";
            conn = MyConnection.getMyConection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            check = ps.executeUpdate() > 0;
            return check;
        } finally {
            closeConnection();
        }
    }

    public FoodDTO findByPrimaryKey(String id) throws Exception {
        FoodDTO dto = null;
        try {
            String sql = "Select * from tbl_Food where FoodID = ?";
            conn = MyConnection.getMyConection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            rs = ps.executeQuery();
            if (rs.next()) {
                String foodID = rs.getString("FoodID");
                String foodName = rs.getString("FoodName");
                float price = rs.getFloat("Price");
                String description = rs.getString("Description");
                String type = rs.getString("Type");
                String status = rs.getString("Status");
                dto = new FoodDTO(foodID, foodName, description, type, status, price);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }

    public boolean update(FoodDTO dto) throws Exception {
        boolean check = false;
        try {

            String sql = "Update tbl_Food Set FoodName = ?, Price = ?, Description = ?, Type = ?, Status = ? where FoodID = ? ";
            conn = MyConnection.getMyConection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getFoodName());
            ps.setFloat(2, dto.getPrice());
            ps.setString(3, dto.getDescription());
            ps.setString(4, dto.getType());
            ps.setString(5, dto.getStatus());
            ps.setString(6, dto.getFoodID());
            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }

    public boolean insert(FoodDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Insert into tbl_Food (FoodID, FoodName, Price, Description, Type, Status) values (?,?,?,?,?,?)";
            conn = MyConnection.getMyConection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getFoodID());
            ps.setString(2, dto.getFoodName());
            ps.setFloat(3, dto.getPrice());
            ps.setString(4, dto.getDescription());
            ps.setString(5, dto.getType());
            ps.setString(6, dto.getStatus());
            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }
}
