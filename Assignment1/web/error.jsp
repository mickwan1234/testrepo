<%-- 
    Document   : error
    Created on : Feb 20, 2019, 2:56:37 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%String error = request.getParameter("ERROR"); %>
        <h1 color="red"><%= error %></h1>
        <a href="index.jsp">Back to Search Page</a>
    </body>
</html>
