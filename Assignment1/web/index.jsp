<%-- 
    Document   : index
    Created on : Feb 19, 2019, 4:16:16 PM
    Author     : mickw
--%>

<%@page import="nghia.errorObject.ErrorObject"%>
<%@page import="nghia.DTO.FoodDTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Food search Page</h1>
        <form action="MainController" method="POST">
            Range of price : <input type="text" name="txtLowerRange" placeholder="Lower range"/>
            <input type="text" name="txtUpperRange" placeholder="Upper range"/>
            <br/>
            <%
                ErrorObject errorobj = (ErrorObject )request.getAttribute("INVALID");
                if (errorobj != null) {
            %>
            <h1 color="red"> <%= errorobj.getRangeError() %></h1>
            <% }
            %>      
            <input type="submit" name="action" value="Search"/>
        </form>
        <form action="insert.jsp" method="POST">
            <input type="hidden" name="txtUpperRange" value="<%= request.getParameter("txtUpperRange")%>"/>
            <input type="hidden" name="txtLowerRange" value="<%= request.getParameter("txtLowerRange")%>"/>
            <input type="submit" value="Insert"/>
        </form>
        <br/>
        <%
            ArrayList<FoodDTO> result = (ArrayList<FoodDTO>) request.getAttribute("INFO");
            if (result != null) {
                if (result.size() > 0) {

        %>

        <table border="1">
            <thead>
                <tr>
                    <th>No</th>
                    <th>FoodID</th>
                    <th>Food Name</th>
                    <th>Price</th>
                    <th>Description</th>
                    <th>type</th>
                    <th>Status</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <% int count = 0;
                for (FoodDTO dto : result) {
                    count++;
            %>
            <tbody>
                <tr>
                    <td><%= count%></td>
                    <td><%= dto.getFoodID()%></td>
                    <td><%= dto.getFoodName()%></td>
                    <td><%= dto.getPrice()%></td>
                    <td><%= dto.getDescription()%></td>
                    <td><%= dto.getType()%></td>
                    <td><%= dto.getStatus()%></td>
                    <td><form action="MainController" method="POST">
                            <input type="hidden" name="txtFoodID" value="<%= dto.getFoodID()%>"/>
                            <input type="hidden" name="txtUpperRange" value="<%= request.getParameter("txtUpperRange")%>"/>
                            <input type="hidden" name="txtLowerRange" value="<%= request.getParameter("txtLowerRange")%>"/>
                            <input type="submit" name="action" value="Update"/>
                        </form></td>
                    <td>
                        <a href="MainController?action=Delete&foodID=<%= dto.getFoodID()%>&txtUpperRange=<%= request.getParameter("txtUpperRange")%>
                           &txtLowerRange=<%= request.getParameter("txtLowerRange")%>">Delete</a>
                    </td>
                </tr>
            </tbody>
            <% } %>
        </table>
        <%
            }
        } else {%>
        <h1 color="red"> Nothing found</h1>
        <% }%>


    </body>
</html>
