<%-- 
    Document   : update
    Created on : Feb 20, 2019, 3:50:04 PM
    Author     : mickw
--%>

<%@page import="nghia.errorObject.ErrorObject"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="nghia.DTO.FoodDTO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="MainController" method="POST">
            <% FoodDTO dto = (FoodDTO) request.getAttribute("INFO");
            System.out.println(dto);
               ErrorObject errorObj = (ErrorObject) session.getAttribute("INVALID");
            %>
            <h1>Insert Page</h1>
            Food ID: <input type="text" name="txtFoodID" />
            <br/>
            <% if (errorObj.getIdError() != null) {%>
            <h1 color="red"><%= errorObj.getIdError()%></h1>
            <% }%>
            Food name: <input type="text" name="txtFoodName"/>
            <br/>
            <% if (errorObj.getNameError()!= null ) {%>
            <h1 color="red"><%= errorObj.getNameError()%></h1>
            <% }%>
            Price: <input type="text" name ="txtPrice"/>
            <br/>
            <% if (errorObj.getPriceError()!=null) {%>
            <h1 color="red"><%= errorObj.getPriceError()%></h1>
            <% }%>
            Description: <input type="text" name ="txtDescription" />
            <br/>
            <% if (errorObj.getDescriptionError() != null) {%>
            <h1 color="red"><%= errorObj.getDescriptionError()%></h1>
            <% }%>
            Type: <input type="text" name="txtType" />
            <br/>
            <% if (errorObj.getTypeError() != null) {%>
            <h1 color="red"><%= errorObj.getTypeError()%></h1>
            <% } %>
            <select name="cblStatus">
                <option selected="true">Con hang</option>
                <option >Het hang</option>
            </select>
            <input type="hidden" value="<%= request.getParameter("txtUpperRange") %>" name ="txtUpperRange"/>
            <input type="hidden" value="<%= request.getParameter("txtLowerRange") %>" name ="txtLowerRange"/>
            <input type="submit" name="action" value="Insert"/>
        </form>
    </body>
</html>
