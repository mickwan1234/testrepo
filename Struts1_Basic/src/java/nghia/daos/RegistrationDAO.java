/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import nghia.connection.MyConnection;
import nghia.dtos.RegistrationDTO;

/**
 *
 * @author mickw
 */
public class RegistrationDAO implements Serializable {

    private Connection conn;
    private PreparedStatement ps;
    private ResultSet rs;

    private void closeConnection() throws Exception {
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (conn != null) {
            conn.close();
        }
    }

    public String checkLogin(String username, String password) throws Exception {
        String role = "failed";
        try {
            String sql = "Select Role from Registration where Username = ? and Password = ?";
            conn = MyConnection.getMyConection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            rs = ps.executeQuery();
            if (rs.next()) {
                role = rs.getString("Role");
            }
        } finally {
            closeConnection();
        }
        return role;
    }
    public List<RegistrationDTO> findByLikeName(String search) throws Exception{
        List<RegistrationDTO> dtos = new ArrayList<>();
        RegistrationDTO dto = null;
        try{
            String sql = "Select Username, Fullname, Role from Registration where Fullname like ?";
            conn = MyConnection.getMyConection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" +search+"%");
            rs = ps.executeQuery();
            while(rs.next()){
                String username = rs.getString("Username");
                String fullname = rs.getString("Fullname");
                String role = rs.getString("Role");
                dto = new RegistrationDTO(username, role, fullname);
                dtos.add(dto);
            }
        }finally{
            closeConnection();
        }
        return dtos;
    }
    
    
}
