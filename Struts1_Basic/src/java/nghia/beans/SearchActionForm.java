/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.beans;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import nghia.daos.RegistrationDAO;
import nghia.dtos.RegistrationDTO;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionServlet;
import org.apache.struts.upload.MultipartRequestHandler;

/**
 *
 * @author mickw
 */
public class SearchActionForm extends org.apache.struts.action.ActionForm {
    
    String search;

    public String getSearch() {
        return search;
    }

    public ActionServlet getServlet() {
        return servlet;
    }

    public MultipartRequestHandler getMultipartRequestHandler() {
        return multipartRequestHandler;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public void setServlet(ActionServlet servlet) {
        this.servlet = servlet;
    }

    public void setMultipartRequestHandler(MultipartRequestHandler multipartRequestHandler) {
        this.multipartRequestHandler = multipartRequestHandler;
    }
    
    public SearchActionForm() {
        super();
        // TODO Auto-generated constructor stub
    }
public List<RegistrationDTO> findByLikeName() throws Exception{
    RegistrationDAO dao = new RegistrationDAO();
    return dao.findByLikeName(search);
}
    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param request The HTTP Request we are processing.
     * @return
     */
    
}
