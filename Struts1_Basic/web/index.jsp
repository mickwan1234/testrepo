<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
    <body>
        <logic:messagesPresent>
            <html:messages id="error">
                <font color="red">
                <ul>
                    <li>${error}</li>
                </ul>
                </font>
                
            </html:messages>
        </logic:messagesPresent>
        <html:form action="Login.do" method="POST">
            Username: <html:text property="username"/><br/>
            Password: <html:password property="password"/><br/>
            <html:submit value="Login"/>
    </html:form>
    </body>
</html>