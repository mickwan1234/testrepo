<%-- 
    Document   : error
    Created on : Feb 26, 2019, 1:59:31 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Error Page</h1>
        <font color="red">
        ${requestScope.ERROR}
        </font>
        <a href="Back.do">Back to Login Page</a>
    </body>
</html>
