/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.StringTokenizer;
import nghia.connection.MyConnection;
import nghia.dtos.CartDTO;
import nghia.dtos.TourDTO;

/**
 *
 * @author mickw
 */
public class TourDAO implements Serializable {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    private void closeConnection() throws Exception {
        if (rs != null) {
            rs.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (conn != null) {
            conn.close();
        }

    }

    public TourDTO findByPrimaryKey(String id) throws Exception {
        TourDTO dto = null;
        try {
            String sql = "Select * where ID = ? and IsDelete = 0";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            if (rs.next()) {
                String name = rs.getString("Name");
                Date time = rs.getDate("Time");
                String image = rs.getString("Image");
                float price = rs.getFloat("Price");
                String regionID = rs.getString("RegionID");
                Date endDate = rs.getDate("EndDate");
                String Description = rs.getString("Description");
                dto = new TourDTO(id, name, image, regionID, Description, time, endDate, false, 0, price);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }

    public boolean insert(TourDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Insert into Tour (ID, Name, time,RegionID, price, Image, IsDelete,EndDate, Description ) values (?,?,?,?,?,?,0,?,?)";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getId());
            ps.setString(2, dto.getName());
            ps.setDate(3, dto.getTime());
            ps.setString(4, dto.getRegionID());
            ps.setFloat(5, dto.getPrice());
            ps.setString(6, dto.getImage());
            ps.setDate(7, dto.getEndTime());
            ps.setString(8, dto.getDescription());
            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }

    public boolean delete(String ID) throws Exception {
        boolean check = false;
        try {
            String sql = "Update Tour set IsDelete = 1 where ID = ?";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, ID);
            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }

    public ArrayList<TourDTO> getLits(String regionIDCbl) throws Exception {
        ArrayList<TourDTO> dtos = new ArrayList<>();
        try {
            String sql = "Select * from Tour where RegionID = ? and IsDelete = 0";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, regionIDCbl);
            rs = ps.executeQuery();
            while (rs.next()) {
                String id = rs.getString("ID");
                String name = rs.getString("Name");
                Date time = rs.getDate("Time");
                Date endTime = rs.getDate("EndDate");
                String image = rs.getString("Image");
                float price = rs.getFloat("Price");
                String regionID = rs.getString("RegionID");
                boolean isDelete = rs.getBoolean("isDelete");
                String Description = rs.getString("Description");
                TourDTO dto = new TourDTO(id, name, image, regionID, Description, time, endTime, isDelete, 0, price);
                dtos.add(dto);
            }
            System.out.println(dtos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return dtos;
    }

    public ArrayList<TourDTO> getLitsByTourName(String regionIDCbl, String tourName) throws Exception {
        ArrayList<TourDTO> dtos = new ArrayList<>();
        try {
            System.out.println(regionIDCbl + " " + tourName);

            String sql = "Select * from Tour where RegionID = ? and Name like ? and IsDelete = 0";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, regionIDCbl);
            ps.setString(2, "%" + tourName + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                String id = rs.getString("ID");
                String name = rs.getString("Name");
                Date time = rs.getDate("Time");
                Date endTime = rs.getDate("EndDate");
                String image = rs.getString("Image");
                float price = rs.getFloat("Price");
                String regionID = rs.getString("RegionID");
                boolean isDelete = rs.getBoolean("isDelete");
                String Description = rs.getString("Description");
                TourDTO dto = new TourDTO(id, name, image, regionID, Description, time, endTime, isDelete, 0, price);
                dtos.add(dto);
            }
            System.out.println(dtos);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return dtos;
    }

    public boolean update(TourDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Update Tour set name = ?, time = ? , endDate = ?, image = ?, price = ?, isDelete = 0, Description = ? where ID = ?";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getName());
            ps.setDate(2, dto.getTime());
            ps.setDate(3, dto.getEndTime());
            ps.setString(4, dto.getImage());
            ps.setFloat(5, dto.getPrice());
            ps.setString(6, dto.getDescription());
            ps.setString(7, dto.getId());
            check = ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }
}
