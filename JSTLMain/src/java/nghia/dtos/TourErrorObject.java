/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.dtos;

import java.io.Serializable;

/**
 *
 * @author mickw
 */
public class TourErrorObject implements Serializable {

    String idError, nameError, timeError, quantityError, priceError, imageLinkError, numberError, error, descriptionError;

    public String getDescriptionError() {
        return descriptionError;
    }

    public void setDescriptionError(String descriptionError) {
        this.descriptionError = descriptionError;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setNumberError(String numberError) {
        this.numberError = numberError;
    }

    public String getNumberError() {
        return numberError;
    }

    public TourErrorObject() {
    }

    public void setIdError(String idError) {
        this.idError = idError;
    }

    public void setNameError(String nameError) {
        this.nameError = nameError;
    }

    public void setTimeError(String timeError) {
        this.timeError = timeError;
    }

    public void setQuantityError(String quantityError) {
        this.quantityError = quantityError;
    }

    public void setPriceError(String priceError) {
        this.priceError = priceError;
    }



    public void setImageLinkError(String imageLinkError) {
        this.imageLinkError = imageLinkError;
    }

    public String getIdError() {
        return idError;
    }

    public String getNameError() {
        return nameError;
    }

    public String getTimeError() {
        return timeError;
    }

    public String getQuantityError() {
        return quantityError;
    }

    public String getPriceError() {
        return priceError;
    }


    public String getImageLinkError() {
        return imageLinkError;
    }

}
