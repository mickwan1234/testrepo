/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mickw
 */
public class TourMainController extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String TOURSEARCH = "ViewController";
    private static final String INSERTTOUR = "InsertTourController";
    private static final String ADD = "AddController";
    private static final String DELETE = "CartDeleteController";
    private static final String UPDATE = "CartUpdateController";
    private static final String BOOK = "TourBookController";
    private static final String VIEWBILL = "TourBillViewController";
    private static final String UPDATETOUR = "UpdateTourController";
    private static final String SEARCHREGION = "TourViewController";
    private static final String DELETETOUR = "TourDeleteController";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            String action = request.getParameter("action");
            System.out.println(action);
            if (action.equals("Insert Tour")) {
                url = INSERTTOUR;
            } else if (action.equals("SearchRegion")) {
                url = TOURSEARCH;
            } else if (action.equals("Add")) {
                url = ADD;
            } else if (action.equals("Delete")) {
                url = DELETE;
            } else if (action.equals("Update")) {
                url = UPDATE;
            } else if (action.equals("Book")) {
                url = BOOK;
            } else if (action.equals("View Bill")) {
                url = VIEWBILL;
            } else if (action.equals("Update Tour")) {
                url = UPDATETOUR;
            } else if (action.equals("Search Tour Region Update")) {
                url = SEARCHREGION;
            } else if (action.equals("Delete Tour")) {
                url = DELETETOUR;
            } else {
                request.setAttribute("ERROR", "Your action is not supported");
            }
        } catch (Exception e) {
            e.printStackTrace();
            log("Erorr at TourMainController: " + e.getMessage());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
