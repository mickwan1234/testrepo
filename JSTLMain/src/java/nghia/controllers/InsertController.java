/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nghia.dtos.RegistrationDTO;
import nghia.dtos.RegistrationErrorObject;
import nghia.models.NghiaBean;

/**
 *
 * @author mickw
 */
public class InsertController extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS = "index.jsp";
    private static final String INVALID = "insert.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {

            String username = request.getParameter("txtUsername");
            String password = request.getParameter("txtPassword");
            String confirm = request.getParameter("txtConfirm");
            String fullname = request.getParameter("txtFullname");
            String phoneNo = request.getParameter("txtPhoneNo");
            String email = request.getParameter("txtEmail");
            String role = "User";
            RegistrationDTO dto = new RegistrationDTO(username, password, fullname, role, phoneNo, email);
            dto.setPassword(password);

            RegistrationErrorObject errorObj = new RegistrationErrorObject();
            boolean valid = true;
            if (username.length() == 0) {
                errorObj.setUsernameError("Username cannot be blank");
                valid = false;
            }
            if (password.length() == 0) {
                errorObj.setPasswordError("password cannot be blank");
                valid = false;
            }
            if (!confirm.equals(password)) {
                errorObj.setConfirmError("Confirm not match");
                valid = false;
            }
            if (fullname.length() == 0) {
                errorObj.setFullnameError("Fullname cannot be blank");
                valid = false;
            }
            if (role.length() == 0) {
                errorObj.setRoleError("role cannto be blank");
                valid = false;
            }
            if (phoneNo.length() == 0) {
                errorObj.setPhoneNoError("Phone Number cannot be blank");
                valid = false;
            }
            if (email.length() == 0) {
                errorObj.setEmailError("Email cannot be blank");
                valid = false;
            }
            if (!phoneNo.matches("^[0-9]{10,15}$")) {
                errorObj.setPhoneNoError("Phone number is error");
                valid = false;
            }
            if (!email.matches("^[0-9a-zA-Z]+@[0-9A-Za-z]+(\\.[0-9A-Za-z]+){1,2}$")) {
                errorObj.setEmailError("Email format is error");
                valid = false;
            }
            if (valid) {
                NghiaBean beans = new NghiaBean();
                beans.setDto(dto);
                if (beans.insert()) {
                    url = SUCCESS;
                } else {
                    request.setAttribute("ERROR", "Insert failed");
                }
            } else {
                request.setAttribute("INVALID", errorObj);
                url = INVALID;
            }
        } catch (Exception e) {
            if (e.getMessage().contains("duplicate")) {
                RegistrationErrorObject errorObj = new RegistrationErrorObject();
                errorObj.setUsernameError("username is existed");
                request.setAttribute("INVALID", errorObj);
                url = INVALID;
            }
            log("Error at InsertController " + e.getMessage());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
