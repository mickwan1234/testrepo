/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Registration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nghia.dtos.RegistrationDTO;
import nghia.dtos.RegistrationErrorObject;
import nghia.models.NghiaBean;

/**
 *
 * @author mickw
 */
public class UpdateController extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS = "SearchController";
    private static final String INVALID = "update.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            HttpSession session = request.getSession();
            RegistrationDTO dtoUser = (RegistrationDTO) session.getAttribute("USER");
            System.out.println("Update Controller: "+ dtoUser);
            session.setAttribute("USER", dtoUser);
            String username = request.getParameter("txtUsername");
            String fullname = request.getParameter("txtFullname");
            String phoneNo = request.getParameter("txtPhoneNo");
            String email = request.getParameter("txtEmail");
            RegistrationDTO dto = new RegistrationDTO(username, null, fullname, "User", phoneNo, email);

            RegistrationErrorObject errorObj = new RegistrationErrorObject();
            boolean valid = true;
            if (fullname.length() == 0) {
                errorObj.setFullnameError("Fullname can't be blank");
                valid = false;
            }
            if(phoneNo.length() == 0){
                errorObj.setPhoneNoError("Phone no cannot be blank");
                valid = false;
            }
            if(email.length() == 0){
                errorObj.setEmailError("Email cannot be blank");
                valid = false;
            }
            if (valid) {
                NghiaBean beans = new NghiaBean();
                beans.setDto(dto);

                if (beans.update()) {
                    url = SUCCESS;
                } else {
                    request.setAttribute("ERROR", "Update failed");
                }
            } else {
                request.setAttribute("INVALID", errorObj);
                request.setAttribute("DTO", dto);
                url = INVALID;
            }
        } catch (Exception e) {
            log("Error at UpdateController " + e.getMessage());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
