/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nghia.daos.TourDAO;
import nghia.dtos.TourDTO;

/**
 *
 * @author mickw
 */
public class TourViewController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            HttpSession session = request.getSession();
            String regionID = request.getParameter("cblRegion");
            System.out.println(regionID);
            String search = request.getParameter("txtSearch");
            System.out.println(search);
            if (search == null) {
                System.out.println("not have search");
                if (regionID == null) {
                    regionID = (String) session.getAttribute("cblRegion");
                }
                session.setAttribute("cblRegion", regionID);
                if (regionID != null) {
                    switch (regionID) {
                        case "South America": {
                            regionID = "SA";
                            break;
                        }
                        case "Asia": {
                            regionID = "As";
                            break;
                        }
                        case "Europe": {
                            regionID = "Eu";
                            break;
                        }
                        case "Africa": {
                            regionID = "Af";
                            break;
                        }
                        case "Australia": {
                            regionID = "Aus";
                            break;
                        }
                    }
                    TourDAO dao = new TourDAO();
                    ArrayList<TourDTO> info = dao.getLits(regionID);
                    System.out.println(info);
                    request.setAttribute("INFO", info);

                }
            } else {
                System.out.println("Have search");
                if (regionID == null) {
                    regionID = (String) session.getAttribute("cblRegion");
                }
                session.setAttribute("cblRegion", regionID);

                if (regionID != null) {

                    switch (regionID) {
                        case "South America": {
                            regionID = "SA";
                            break;
                        }
                        case "Asia": {
                            regionID = "As";
                            break;
                        }
                        case "Europe": {
                            regionID = "Eu";
                            break;
                        }
                        case "Africa": {
                            regionID = "Af";
                            break;
                        }
                        case "Australia": {
                            regionID = "Aus";
                            break;
                        }
                    }
                    TourDAO dao = new TourDAO();
                    ArrayList<TourDTO> info = dao.getLitsByTourName(regionID, search);
                    System.out.println(info);
                    request.setAttribute("INFO", info);
                    session.setAttribute("txtSearch", search);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            log("Error at View Controller: " + e.getMessage());
        } finally {
            request.getRequestDispatcher("searchTourPage.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
