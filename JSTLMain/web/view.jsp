<%-- 
    Document   : view
    Created on : Mar 4, 2019, 2:50:39 PM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${sessionScope.USER.role == null}">
            <c:redirect url="index.jsp"/>
        </c:if>
        <h1>${sessionScope.USER.fullname}'s Cart</h1>

        <c:if test="${sessionScope.shoppingCart != null}">
            <c:if test="${not empty sessionScope.shoppingCart }" var="check">
                <form action="TourMainController" method="POST">
                    <table border="1">
                        <thead>
                            <tr>                                                         
                                <th>Username</th>
                                <th>TourID</th>
                                <th>Tour name</th>
                                <th>Quantity</th>
                                <th>TotalPrice</th>
                                <th>Date</th>
                                <th>Delete</th>
                                <th>Update</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="dto" items="${sessionScope.shoppingCart.bill}">
                                <tr>
                                    <td><input type="text" readonly="true"value="${sessionScope.USER.username}"/></td>
                                    <td><input type="text" readonly="true"value="${dto.value.id}" name="txtID"/></td>
                                    <td><input type="text" readonly="true" value="${dto.value.name}"/></td>
                                    <td><input type="text" name="txtQuantity" value="${dto.value.quantity}"/>
                                        <font color="red">${requestScope.INVALID.quantityError}</font>
                                    </td>
                                    <td><input type="text" readonly="true" value="${dto.value.price} a person" /></td>
                                    <td><input type="text" readonly="true" value="${dto.value.time}"/></td>
                                    <td>
                                        <input type="hidden" name="cartElement" value="${dto.value.id}"/>
                                        <input type="hidden" name="shoppingCart" value="${sessionScope.shoppingCart}"/>
                                        <input type="submit" name="action" value="Delete"/>
                                    </td>
                                    <td>
                                        <input type="hidden" name="cartElement" value="${dto.value.id}"/>
                                        <input type="hidden" name="shoppingCart" value="${sessionScope.shoppingCart}"/>
                                        <input type="submit" name="action" value="Update"/></td>
                                </tr>
                            </c:forEach>
                            <tr><td>Total price : </td>
                                <td style="width: 100%"><c:out value="${shoppingCart.total}"/></td>
                            </tr>
                            <tr>
                                <td style="width: 100%"><a href="user.jsp">Continue shopping</a></td>
                            </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="shoppingCart" value="${sessionScope.shoppingCart}"/>
                    <input type="submit" name="action" value="Book"/>
                </form>
            </c:if>
        </c:if>
        <c:if test="${sessionScope.shoppingCart == null}">
            <h2>Nothing in cart</h2>
        </c:if>
    </body>
</html>
