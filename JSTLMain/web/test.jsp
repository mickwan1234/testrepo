<%-- <
    Document   : test
    Created on : Feb 19, 2019, 12:57:42 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <%@include file="body.jsp" %>
        <%@include file="colright.jsp" %>
        <%@include file="footer.jsp" %>
    </body>
</html>
