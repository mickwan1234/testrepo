<%-- 
    Document   : update
    Created on : Feb 14, 2019, 1:04:03 PM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Update page</h1>
    <c:if test="${sessionScope.USER.role == null}">
        <c:redirect url="index.jsp"/>
    </c:if>
    <form action="MainController" method="POST">
        Username: <input type="text" name="txtUsername" value="${requestScope.DTO.username}" readonly="true"/>
        </br>
        Fullname: <input type ="text" name ="txtFullname" value="${requestScope.DTO.fullname}"/>
        <font color ="red">
        ${requestScope.INVALID.fullnameError}
        </font>
        </br>
        Email: <input type="text" name="txtEmail" value="${requestScope.DTO.email}"/>
        <font color="red">
        ${requestScope.INVALID.emailError}
        </font>
        <br/>
        PhoneNo: <input type="text" name="txtPhoneNo" value="${requestScope.DTO.phoneNo}"/>
        <font color="red">
        ${requestScope.INVALID.phoneNoError}
        </font>
        <input type="hidden" name="txtSearch" value="${param.txtSearch}"/>
        <input type="submit" name="action" value="Update"/>
    </form>
</body>
</html>
