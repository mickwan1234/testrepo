<%-- 
    Document   : admin
    Created on : Feb 12, 2019, 3:18:55 PM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${sessionScope.USER.role == null}">
            <c:redirect url="index.jsp"/>
        </c:if>
        <div style="margin-left: 70%;size: 500px">
            <form action="MainController" method="POST">
                <input type="submit" name="action" value="Logout"/>
            </form></div>
        <h1>Hello ${sessionScope.USER.fullname}</h1>
        <h2>Search Account</h2>
        <form action="MainController" method="POST">
            Fullname: <input type="text" name ="txtSearch"/>
            <br/>
            <input type="hidden" name="USER" value="${sessionScope.USER}"/>
            <input type="submit" value="Search" name="action"/>
        </form>
            <br/>
            <form action="searchTourPage.jsp" method="POST">
                <input type="submit" value="Tour Search"/>
            </form>
        <form action="tourInsert.jsp" method="POST">
            <input type="hidden" name="USER" value="${sessionScope.USER}" />
            <input type="submit" name="action" value="Insert new tour"/>
        </form>
        <c:if test="${requestScope.INFO != null}">
            <c:if test="${not empty requestScope.INFO}" var="checkData">
                <table border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Username</th>
                            <th>Fullname</th>
                            <th>Role</th>
                            <th>Delete</th>
                            <th>Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="dto" items="${requestScope.INFO}" varStatus="counter">
                            <tr>
                                <td>${counter.count}</td>
                                <td>${dto.username}</td>
                                <td>${dto.fullname}</td>
                                <td>${dto.role}</td>
                                <td><c:url var="DeleteLink" value="MainController">
                                        <c:param name="action" value="Delete"/>
                                        <c:param name="username" value="${dto.username}"/>
                                        <c:param name ="txtSearch" value="${param.txtSearch}"/>
                                    </c:url>
                                    <a href="${DeleteLink}">Delete</a>
                                </td>
                                <td>
                                    <form action="MainController" method="POST">
                                        <input type="hidden" name="username" value="${dto.username}"/>
                                        <input type="hidden" name="txtSearch" value="${param.txtSearch}"/>
                                        <input type="submit" name="action" value="Edit"/>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${!checkData}">
                No record found
            </c:if>
        </c:if>

    </body>
</html>
