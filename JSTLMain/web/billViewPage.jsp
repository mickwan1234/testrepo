<%-- 
    Document   : billViewPage
    Created on : Mar 10, 2019, 10:43:13 AM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1> ${sessionScope.USER.fullname} View Bill Page</h1>
        <c:if test="${requestScope.INFO != null}">
            <c:if test="${not empty requestScope.INFO}" var="check">
                <table border="1">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>TourID</th>
                            <th>Tour Name</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="dto" items="${requestScope.INFO}">
                        <tr>
                            <td>${dto.id}</td>
                            <td>${dto.username}</td>
                            <td>${dto.tourId}</td>
                            <td>${dto.tourName}</td>
                            <td>${dto.quantity}</td>
                            <td>${dto.totalPrice}</td>
                            <td>${dto.date}</td>
                        </tr>
                        </c:forEach>
                    </tbody>
                </table>

            </c:if>
        </c:if>
    </body>
</html>
