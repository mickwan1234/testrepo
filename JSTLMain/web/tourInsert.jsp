
<%@page import="java.util.Calendar"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${sessionScope.USER.role == null}">
            <c:redirect url="index.jsp"/>
        </c:if>
        <h1>Tour Insert Page</h1>
        <form action="TourMainController" method="POST">
            TourID: <input type="tex" name="txtID"/> <font color="red">${requestScope.INVALID.idError}</font><br/>
            TourName: <input type ="text" name="txtName"/><font color="red">${requestScope.INVALID.nameError}</font><br/>
            Description: <textarea name="txtDescription" value="${entry.description}"></textarea><font color="red">${requestScope.INVALID.descriptionError}</font><br/>
            Time: <input type="date" name="cblDate"/>
            <font color="red">${requestScope.INVALID.timeError}</font><br/>
            Time End: <input type="date" name="cblEndDate"/>
            <font color="red">${requestScope.INVALID.timeError}</font><br/>
            Price: <input type="text" name="txtPrice"/><font color="red">${requestScope.INVALID.priceError}${requestScope.INVALID.numberError}</font><br/>
            Image link: <input type="text" name="txtImageLink"/><font color="red">${requestScope.INVALID.imageLinkError}</font><br/>
            
            <select name="cblRegionID">
                <option>Eu (Europe)</option>
                <option>As (Asia)</option>
                <option>Af (Africa)</option>
                <option>SA (South America)</option>
                <option>Aus (Australia)</option>
            </select>
            <input type="submit" value="Insert Tour" name="action"/>${requestScope.INVALID.error}
        </form>
    </body>
</html>
