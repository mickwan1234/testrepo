<%-- 
    Document   : insert
    Created on : Feb 14, 2019, 1:35:05 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Registration Page</h1>
        <form action="MainController" method="POST">
            Username: <input type="text" name="txtUsername" value="${param.username}"/>
            <font color="red">
            ${requestScope.INVALID.usernameError}
            </font>
            <br/>
            Password: <input type="password" name="txtPassword"/>
            <font color="red">
            ${requestScope.INVALID.passwordError}
            </font>
            <br/>
            Confirm Password: <input type="password" name="txtConfirm"/>
            <font color="red">
            ${requestScope.INVALID.confirmError}
            </font>
            <br/>
            Fullname: <input type="text" name="txtFullname" value="${param.fullname}"/>
            <font color="red">
            ${requestScope.INVALID.fullnameError}
            </font>
            <br/>
            Email: <input type="text" name="txtEmail"/>
            <font color="red">
            ${requestScope.INVALID.emailError}
            </font>
            <br/>
            PhoneNo: <input type="text" name="txtPhoneNo"/>
            <font color="red">
            ${requestScope.INVALID.phoneNoError}
            </font>
            <input type="hidden" name="USER" value="${sessionScope.USER}"/>
            <input type="submit" name="action" value="Insert"/>
        </form>
    </body>
</html>
