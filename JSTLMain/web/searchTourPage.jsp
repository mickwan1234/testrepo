<%-- 
    Document   : user
    Created on : Feb 28, 2019, 4:13:04 PM
    Author     : mickw
--%>

<%@page import="nghia.dtos.TourDTO"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body> 
        <c:if test="${sessionScope.USER.role == null}">
            <c:redirect url="index.jsp"/>
        </c:if>
        <h1>Welcome to Book tour Page ${sessionScope.USER.fullname}</h1> 
        <h2>Choose your destination region: </h2>
        <form action="TourMainController" method="POST">
            <select name="cblRegion">
                <option>Select region....</option>
                <option>Asia</option>
                <option>Europe</option>
                <option>South America</option>
                <option>Africa</option>
                <option>Australia</option>
            </select>
            <input type="text" name="txtSearch" value="${sessionScope.txtSearch}"/>
            <input type="hidden" name="shoppingCart" value="${sessionScope.shoppingCart}"/>
            <input type="submit" name="action" value="Search Tour Region Update"/>
        </form>
        <br/>
        <div style="margin-left: 70%;size: 500px;float: top">
            <form action="MainController" method="POST">
                <input type="submit" name="action" value="Logout"/>
            </form>
        </div>
        <c:if test="${requestScope.INFO != null}">
            <c:if test="${not empty requestScope.INFO}" var="check">
                <table border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>TourID</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Time</th>
                            <th>End Time</th>
                            <th>RegionID</th>
                            <th>image</th>
                            <th>Update</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="entry" items="${requestScope.INFO}" varStatus="counter">
                            <tr>
                                <td>${counter.count}</td>
                                <td>${entry.id}
                                </td>
                                <td>${entry.name}
                                </td>
                                <td>
                                    ${entry.description}
                                </td>
                                <td>${entry.price}
                                </td>
                                <td>${entry.time}
                                </td>
                                <td>${entry.endTime}</td>
                                <td>
                                    ${entry.regionID}
                                </td>
                                <td><img src="${entry.image}" width="200" height="100">
                                </td>
                                <td>
                                    <form action="updateTour.jsp" method="POST">
                                        <input type="hidden" value="${entry.id}" name="txtID"/>
                                        <input type="hidden" name="txtName" value="${entry.name}"/>
                                        <input type="hidden" name="txtDescription" value="${entry.description}"/>
                                        <input type="hidden" name="txtPrice" value="${entry.price}"/>
                                        <input type="hidden" name="txtTime" value="${entry.time}"/>
                                        <input type="hidden" name="txtEndTime" value="${entry.endTime}"/>
                                        <input type="hidden" name="txtImage" value="${entry.image}"/>
                                        <input type="hidden" name="txtQuantity" value="${sessionScope.cblRegion}"/>
                                        <input type="hidden" name="txtRegionID" value="${entry.regionID}"/>
                                        <input type="hidden" name="cblRegion" value="${param.cblRegion}"/>
                                        <input type="submit" name="action" value="Update Tour"/>
                                    </form>
                                </td>
                                <td>
                                    <form action="TourMainController" method="POST">
                                        <input type="hidden" name="txtSearch"  value="${param.txtSearch}"/>
                                        <input type="hidden" value="${entry.id}" name="txtID"/>
                                        <input type="submit" name="action" value="Delete Tour"/>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${!check}">
                there is no tour available
            </c:if>
        </c:if>

    </body>
</html>
