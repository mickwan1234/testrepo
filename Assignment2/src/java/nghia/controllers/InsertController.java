/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nghia.beans.Beans;
import nghia.dtos.AccessoryDTO;
import nghia.errorObject.ErrorObject;

/**
 *
 * @author mickw
 */
public class InsertController extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String SUCCESS = "SearchController";
    private static final String INVALID = "insert.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        boolean delete;
        ErrorObject errorObj = new ErrorObject();
        try {
            HttpSession session = request.getSession();
            String action = (String) session.getAttribute("Action");
            session.setAttribute("Action", action);
            String accessoryID = request.getParameter("txtAccessoryID");
            String accessoryName = request.getParameter("txtAccessoryName");
            String brand = request.getParameter("txtBrand");
            String description = request.getParameter("txtDescription");
            String price = request.getParameter("txtPrice");
            String isDeleted = request.getParameter("cblDeleted");
            if (isDeleted.equals("True")) {
                delete = true;
            } else {
                delete = false;
            }

            boolean valid = true;
            if (accessoryID.length() == 0) {
                errorObj.setAccessoryIDError("Id cannot be blank");
                valid = false;
            }
            if (accessoryName.length() == 0) {
                errorObj.setAccessoryNameError("Name cannot be blank");
                valid = false;
            }
            if (brand.length() == 0) {
                errorObj.setBrandError("Brand cannot be blank");
                valid = false;
            }
            if (description.length() == 0) {
                errorObj.setDescriptionError("Brand cannot be blank");
                valid = false;
            }
            if (price.length() == 0) {
                errorObj.setPriceError("Price cannot be blank");
                valid = false;
            }
            if (Float.parseFloat(price) <= 0) {
                errorObj.setPriceError("Price cannot smaller than 0");
                valid = false;
            }
            if (valid) {
                AccessoryDTO dto = new AccessoryDTO(accessoryID, accessoryName, brand, description, Float.parseFloat(price), delete);
                Beans beans = new Beans();
                beans.setDto(dto);
                if (beans.insert()) {
                    url = SUCCESS;
                } else {
                    request.setAttribute("ERROR", "Insert failed");
                }
            } else {
                request.setAttribute("INVALID", errorObj);
                url = INVALID;
            }
        } catch (Exception e) {
            if (e.toString().contains("NumberFormat")) {
                errorObj.setPriceError("Not a number");
                request.setAttribute("INVALID", errorObj);
                url = INVALID;

            }
            if (e.toString().contains("Duplicate")) {
                errorObj.setAccessoryIDError("ID duplicated");
                request.setAttribute("INVALID", errorObj);
                url = INVALID;
            }
            log("Error at InsertContrller: " + e.getMessage());
        } finally {
            HttpSession session = request.getSession();
            session.invalidate();
            request.getRequestDispatcher(url).forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
