/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.errorObject;

import java.io.Serializable;

/**
 *
 * @author mickw
 */
public class ErrorObject implements Serializable {

    public String searchBoxError, accessoryIDError, accessoryNameError, brandError, descriptionError, priceError;

    public void setAccessoryIDError(String accessoryIDError) {
        this.accessoryIDError = accessoryIDError;
    }

    public void setAccessoryNameError(String accessoryNameError) {
        this.accessoryNameError = accessoryNameError;
    }

    public void setBrandError(String brandError) {
        this.brandError = brandError;
    }

    public void setDescriptionError(String descriptionError) {
        this.descriptionError = descriptionError;
    }

    public void setPriceError(String priceError) {
        this.priceError = priceError;
    }

    public String getAccessoryIDError() {
        return accessoryIDError;
    }

    public String getAccessoryNameError() {
        return accessoryNameError;
    }

    public String getBrandError() {
        return brandError;
    }

    public String getDescriptionError() {
        return descriptionError;
    }

    public String getPriceError() {
        return priceError;
    }

    public ErrorObject() {
    }

    public String getSearchBoxError() {
        return searchBoxError;
    }

    public void setSearchBoxError(String searchBoxError) {
        this.searchBoxError = searchBoxError;
    }

}
