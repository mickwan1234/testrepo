/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.dtos;

import java.io.Serializable;

/**
 *
 * @author mickw
 */
public class AccessoryDTO implements Serializable{
    String accessoryID, accessoryName, brand, description;
    float price;
    boolean delete;

    public AccessoryDTO(String accessoryID, String accessoryName, String brand, String description, float price, boolean delete) {
        this.accessoryID = accessoryID;
        this.accessoryName = accessoryName;
        this.brand = brand;
        this.description = description;
        this.price = price;
        this.delete = delete;
    }

    public String getAccessoryID() {
        return accessoryID;
    }

    public String getAccessoryName() {
        return accessoryName;
    }

    public String getBrand() {
        return brand;
    }

    public String getDescription() {
        return description;
    }

    public float getPrice() {
        return price;
    }

    public boolean isDelete() {
        return delete;
    }

    public void setAccessoryID(String accessoryID) {
        this.accessoryID = accessoryID;
    }

    public void setAccessoryName(String accessoryName) {
        this.accessoryName = accessoryName;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setDelete(boolean delete) {
        this.delete = delete;
    }

    @Override
    public String toString() {
        return "AccessoryDTO{" + "accessoryID=" + accessoryID + ", accessoryName=" + accessoryName + ", brand=" + brand + ", description=" + description + ", price=" + price + ", delete=" + delete + '}';
    }
    
}
