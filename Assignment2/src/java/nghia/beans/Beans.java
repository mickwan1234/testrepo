/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.beans;

import java.io.Serializable;
import java.util.List;
import nghia.daos.AccessoryDAO;
import nghia.dtos.AccessoryDTO;

/**
 *
 * @author mickw
 */
public class Beans implements Serializable {

    String brand, key;
    AccessoryDTO dto;

    public void setKey(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setDto(AccessoryDTO dto) {
        this.dto = dto;
    }

    public AccessoryDTO getDto() {
        return dto;
    }

    public Beans() {
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public List<AccessoryDTO> findByBrand() throws Exception {
        AccessoryDAO dao = new AccessoryDAO();
        return dao.searchByBrand(brand);
    }

    public boolean insert() throws Exception {
        AccessoryDAO dao = new AccessoryDAO();
        return dao.insert(dto);
    }
    public AccessoryDTO findbyPrimaryKey() throws Exception{
        AccessoryDAO dao= new AccessoryDAO();
        return dao.findByPrimary(key);
    }
    public boolean update() throws Exception{
        AccessoryDAO dao = new AccessoryDAO();
        return dao.update(dto);
    }
    public boolean delete() throws Exception{
        AccessoryDAO dao = new AccessoryDAO();
        return dao.delete(key);
    }
}
