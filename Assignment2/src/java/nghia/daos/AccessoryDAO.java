/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.daos;

import connect.MyConnection;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import nghia.dtos.AccessoryDTO;

/**
 *
 * @author mickw
 */
public class AccessoryDAO implements Serializable {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    private void closeConnection() throws Exception {
        if (conn != null) {
            conn.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public List<AccessoryDTO> searchByBrand(String brand) throws Exception {
        List<AccessoryDTO> dtos = new ArrayList<>();;
        try {
            String sql = "Select * from tbl_Accessory where Brand = ?  and isDelete = 0";
            conn = MyConnection.getMyConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, brand);
            rs = ps.executeQuery();
            while (rs.next()) {
                String AccessoryID = rs.getString("AccessoryID");
                String AccessoryName = rs.getString("AccessoryName");
                String brandName = rs.getString("Brand");
                float price = rs.getFloat("Price");
                String description = rs.getString("Description");
                boolean isDelete = rs.getBoolean("isDelete");
                AccessoryDTO dto = new AccessoryDTO(AccessoryID, AccessoryName, brand, description, price, isDelete);
                dtos.add(dto);
            }
        } finally {
            closeConnection();
        }
        return dtos;
    }

    public boolean insert(AccessoryDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Insert into tbl_Accessory (AccessoryID, AccessoryName, Brand, Price, Description, IsDelete) values(?,?,?,?,?,?)";
            conn = MyConnection.getMyConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getAccessoryID());
            ps.setString(2, dto.getAccessoryName());
            ps.setString(3, dto.getBrand());
            ps.setFloat(4, dto.getPrice());
            ps.setString(5, dto.getDescription());
            ps.setBoolean(6, dto.isDelete());
            check = ps.executeUpdate() > 0;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }

    public AccessoryDTO findByPrimary(String key) throws Exception {
        AccessoryDTO dto = null;
        try {
            String sql = "Select * from tbl_Accessory where AccessoryID = ?";
            conn = MyConnection.getMyConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, key);
            rs = ps.executeQuery();
            if (rs.next()) {
                String AccessoryID = rs.getString("AccessoryID");
                String AccessoryName = rs.getString("AccessoryName");
                String brandName = rs.getString("Brand");
                float price = rs.getFloat("Price");
                String description = rs.getString("Description");
                boolean isDelete = rs.getBoolean("isDelete");
                dto = new AccessoryDTO(AccessoryID, AccessoryName, brandName, description, price, isDelete);
            }
        } finally {
            closeConnection();
        }
        return dto;
    }

    public boolean update(AccessoryDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Update tbl_Accessory set AccessoryName = ?, Brand = ?, Description = ?, Price = ?, IsDelete = ? where AccessoryID = ?";
            conn = MyConnection.getMyConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getAccessoryName());
            ps.setString(2, dto.getBrand());
            ps.setString(3, dto.getDescription());
            ps.setFloat(4, dto.getPrice());
            ps.setBoolean(5, dto.isDelete());
            ps.setString(6, dto.getAccessoryID());
            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }

    public boolean delete(String key) throws Exception {
        boolean check = false;
        try {
            String sql = "Update tbl_Accessory set IsDelete = 1 where AccessoryID = ? ";
            conn = MyConnection.getMyConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, key);
            check = ps.executeUpdate() > 0;
            System.out.println(check);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }
}
