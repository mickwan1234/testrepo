<%-- 
    Document   : error
    Created on : Feb 25, 2019, 12:03:15 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Error Page</h1>
        <h1 color="red">${requestScope.ERROR}</h1>
    </body>
</html>
