<%-- 
    Document   : index
    Created on : Feb 25, 2019, 11:50:25 AM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Search</h1>
        <form action ="MainController" method="POST">
            <input type="text" name="txtSearch"/>
            <br/>
            ${requestScope.INVALID.searchBoxError}
            <input type="submit" name="action" value="Search"/>
        </form>
        <br/>
        <form action="insert.jsp" method="POST">
            <input type="hidden" name="txtSearch" value="${param.txtSearch}"/>
            <c:set var="Action" value="Insert" scope="session" />
            <input type="submit" name="action" value="Insert" />
        </form>
        <c:if test="${requestScope.INFO != null}">
            <c:if test="${not empty requestScope.INFO}" var="checkData">
                <table border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Accessory ID</th>
                            <th>Accessory Name</th>
                            <th>Brand</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Is Deleted</th>
                            <th>Delete</th>
                            <th>Update</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="dto" items="${requestScope.INFO}" varStatus="Counter">
                            <tr>
                                <td>${Counter.count}</td>
                                <td>${dto.accessoryID}</td>
                                <td>${dto.accessoryName}</td>
                                <td>${dto.brand}</td>
                                <td>${dto.price}</td>
                                <td>${dto.description}</td>
                                <td>${dto.delete}</td>
                                <td><form action="MainController" method="POST">
                                        <input type="hidden" name="txtAccessoryID" value="${dto.accessoryID}"/>
                                        <input type="hidden" name="txtSearch" value="${param.txtSearch}"/>
                                        <input type="submit" name="action" value="Delete"/>
                                    </form></td>
                                <td><form action="MainController" method="POST">
                                        <input type="hidden" name="txtAccessoryID" value="${dto.accessoryID}"/>
                                        <input type="hidden" name="txtSearch" value="${param.txtSearch}"/>
                                        <input type="submit" name="action" value="Update"/>
                                    </form></td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${!checkData}">
                <h1 color="red">Nothing found</h1>
            </c:if>
        </c:if>
    </body>
</html>
