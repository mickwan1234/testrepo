<%-- 
    Document   : insert
    Created on : Feb 25, 2019, 2:57:22 PM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>${sessionScope.Action} Accessory</h1>
        <div style="margin-left: 40%; margin-top: 10%">
            <form action="MainController" method="POST">
                <c:if test="${ empty requestScope.INFO }" var="switchMethod">
                    Accessory ID: <input type="text" name="txtAccessoryID"/>
                    <h5 style="color: red">${requestScope.INVALID.accessoryIDError}</h5>
                </c:if>
                <c:if test="${!switchMethod}" >
                    Accessory ID: <input type="text" name="txtAccessoryID" value="${requestScope.INFO.accessoryID}" readonly="true"/>
                </c:if>
                <br/>
                Accessory Name: <input type="text" name="txtAccessoryName" value="${requestScope.INFO.accessoryName}"/>
                <h5 style="color: red">${requestScope.INVALID.accessoryNameError}</h5>
                <br/>
                Brand: <input type="text" name="txtBrand" value="${requestScope.INFO.brand}"/>
                <h5 style="color: red">${requestScope.INVALID.brandError}</h5>
                <br/>
                Description: <input type="text" name="txtDescription" value="${requestScope.INFO.description}"/>
                <h5 style="color: red">${requestScope.INVALID.descriptionError}</h5>
                <br/>
                price: <input type="text" name="txtPrice" value="${requestScope.INFO.price}"/>
                <h5 style="color: red">${requestScope.INVALID.priceError}</h5>
                <br/>
                Deleted: <select name="cblDeleted">
                    <c:if test="${requestScope.INFO.delete == true}" var="check">
                        <option selected="true">True</option>
                        <option>False</option>
                    </c:if>
                    <c:if test="${!check}">
                        <option >True</option>
                        <option selected="true">False</option>
                    </c:if>
                </select>
                <input type="hidden" name="txtSearch" value="${param.txtSearch}"/>
                <br/>  
                <c:if test="${ empty requestScope.INFO }" var="test">
                    <input type="submit" name="action" value="Insert"/>
                </c:if>
                <c:if test="${!test}">
                    <input type="submit" name="action" value="Edit"/> 
                </c:if>

            </form>
        </div>
    </body>
</html>
