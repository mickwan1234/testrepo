/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.errorObj;

import java.io.Serializable;

/**
 *
 * @author mickw
 */
public class ErrorObject implements Serializable{
    private String idError, nameError, descriptionError, priceError,odoError;

    public String getIdError() {
        return idError;
    }

    public void setIdError(String idError) {
        this.idError = idError;
    }

    public String getNameError() {
        return nameError;
    }

    public void setNameError(String nameError) {
        this.nameError = nameError;
    }

    public String getDescriptionError() {
        return descriptionError;
    }

    public void setDescriptionError(String descriptionError) {
        this.descriptionError = descriptionError;
    }

    public String getPriceError() {
        return priceError;
    }

    public void setPriceError(String priceError) {
        this.priceError = priceError;
    }

    public String getOdoError() {
        return odoError;
    }

    public void setOdoError(String odoError) {
        this.odoError = odoError;
    }
    
}
