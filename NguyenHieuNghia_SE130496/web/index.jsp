<%-- 
    Document   : index
    Created on : Mar 12, 2019, 12:40:13 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Add Bike Page</h1>
        <form action="MainController" method="POST">
            ID: <input type="text" name="txtID"/>
            <font color="red">${requestScope.INVALID.idError}</font>
            <br/>
            Name: <input type="text" name="txtName"/>
            <font color="red">${requestScope.INVALID.nameError}</font>
            <br/>
            Description: <input type="text" name="txtDescription"/>
            <font color="red">${requestScope.INVALID.descriptionError}</font>
            <br/>
            Price: <input type="text" name="txtPrice"/>
            <font color="red">${requestScope.INVALID.priceError}</font>
            <br/>
            ODO: <input type="text" name="txtODO"/>
            <font color="red">${requestScope.INVALID.odoError}</font>
            <br/>
            <input type="submit" name="action" value="Add Bike"/>
        </form>
            <a href="searchPage.jsp">Search</a>
    </body>
</html>
