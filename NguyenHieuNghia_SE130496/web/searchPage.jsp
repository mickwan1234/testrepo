<%-- 
    Document   : index
    Created on : Feb 25, 2019, 11:50:25 AM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Search</h1>
        <form action ="MainController" method="POST">
            From: <input type="text" name="txtSearch"/>
            <br/>
            To: <input type="text" name="txtSearch2"/>
            ${requestScope.INVALID.searchBoxError}
            <input type="submit" name="action" value="Search"/>
        </form>
        <br/>
        <c:if test="${requestScope.INFO != null}">
            <c:if test="${not empty requestScope.INFO}" var="checkData">
                <table border="1">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>id</th>
                            <th>Name</th>
                            <th>Description</th>
                            <th>Price</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="dto" items="${requestScope.INFO}" varStatus="Counter">
                            <tr>
                                <td>${Counter.count}</td>
                                <td>${dto.id}</td>
                                <td>${dto.name}</td>
                                <td>${dto.price}</td>
                                <td>${dto.description}</td>
                                <td>${dto.odo}</td>
                                <td>${dto.delete}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${!checkData}">
                <h1 color="red">Nothing found</h1>
            </c:if>
        </c:if>
    </body>
</html>
