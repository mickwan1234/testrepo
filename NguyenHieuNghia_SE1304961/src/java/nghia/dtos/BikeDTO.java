/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.dtos;

import java.io.Serializable;

/**
 *
 * @author mickw
 */
public class BikeDTO implements Serializable{
    String id, name, description;
    float price,odo;
    boolean isDelete;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public float getODO() {
        return odo;
    }

    public void setODO(float odo) {
        this.odo = odo;
    }

    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }

    @Override
    public String toString() {
        return "BikeDTO{" + "id=" + id + ", name=" + name + ", description=" + description + ", price=" + price + ", ODO=" + odo + ", isDelete=" + isDelete + '}';
    }

    public BikeDTO(String id, String name, String description, float price, float odo, boolean isDelete) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.odo = odo;
        this.isDelete = isDelete;
    }
    
}
