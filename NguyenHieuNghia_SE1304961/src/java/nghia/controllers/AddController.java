/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import nghia.daos.BikeDAO;
import nghia.dtos.BikeDTO;
import nghia.errorObj.ErrorObject;

/**
 *
 * @author mickw
 */
public class AddController extends HttpServlet {

    private static String ERROR = "error.jsp";
    private static String SUCCESS = "index.jsp";
    private static String INVALID = "index.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        ErrorObject errorObj = new ErrorObject();
        try {
            String id = request.getParameter("txtID");
            String name = request.getParameter("txtName");
            String description = request.getParameter("txtDescription");
            String price = request.getParameter("txtPrice");
            String odo = request.getParameter("txtODO");
            boolean valid = false;
            if (id.length() == 0) {
                errorObj.setIdError("ID cannot be blank");
                valid = false;
            }
            if (name.length() == 0) {
                errorObj.setNameError("name cannot be blank");
                valid = false;
            }
            if (price.length() == 0) {
                errorObj.setPriceError("price cannot be blank");
                valid = false;
            }
            if (odo.length() == 0) {
                errorObj.setOdoError("odo cannot be blank");
                valid = false;
            }
            if (description.length() == 0) {
                errorObj.setDescriptionError("description cannot be blank");
                valid = false;
            }
            if (Float.parseFloat(price) <= 0) {
                errorObj.setPriceError("Price cannot less than 0");
                valid = false;
            }
            if (Float.parseFloat(odo) <= 0) {
                errorObj.setOdoError("odo canno less than 0");
                valid = false;
            }
            if (valid) {
                BikeDTO dto = new BikeDTO(id, name, description, Float.parseFloat(price), Float.parseFloat(odo), valid);
                BikeDAO dao = new BikeDAO();
                if (dao.add(dto)) {
                    url = SUCCESS;
                } else {
                    request.setAttribute("ERROR", "add failed");
                }
            } else {
                url = INVALID;
                request.setAttribute("INVALID", errorObj);
            }
        } catch (Exception e) {
            if (e.toString().contains("duplicate")) {
                errorObj.setIdError("id duplicate");
                url = INVALID;
            }
            if (e.toString().contains("NumberFormat")) {
                errorObj.setPriceError("Number is wrong format");
                errorObj.setOdoError("Number wrong format");
                url = INVALID;
            }
            log("ERORR at AddController: " + e.getMessage());
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
