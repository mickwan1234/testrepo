/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import nghia.connection.MyConnection;
import nghia.dtos.BikeDTO;

/**
 *
 * @author mickw
 */
public class BikeDAO implements Serializable {

    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    private void closeConnection() throws Exception {
        if (conn != null) {
            conn.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public boolean add(BikeDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Insert into TBL_Bike(ID,Name,Description, Price,ODO,IsDelete) "
                    + "Values(?,?,?,?,?,0)";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getId());
            ps.setString(2, dto.getName());
            ps.setString(3, dto.getDescription());
            ps.setFloat(4, dto.getPrice());
            ps.setFloat(5, dto.getODO());
            check = ps.executeUpdate() > 0;
            System.out.println(check);
        }catch(Exception e){
        e.printStackTrace();
        } finally {
            closeConnection();
        }
        return check;
    }

    public ArrayList<BikeDTO> search(float odo1, float odo2) throws Exception {
        ArrayList<BikeDTO> dtos = new ArrayList<>();
        try {
            String sql = "Select Id, Name,Description, price, odo where odo between ? and ? and isDelete = 0";

            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setFloat(1, odo1);
            ps.setFloat(2, odo2);
            rs = ps.executeQuery();

            while (rs.next()) {
                String id = rs.getString("ID");
                String name = rs.getString("Name");
                String des = rs.getString("Description");
                float price = rs.getFloat("Price");
                float odo = rs.getFloat("ODO");
                BikeDTO dto = new BikeDTO(id, name, des, price, odo, false);
                dtos.add(dto);
            }
        } finally {
            closeConnection();
        }
        return dtos;
    }
}
