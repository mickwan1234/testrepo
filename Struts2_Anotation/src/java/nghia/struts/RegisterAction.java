/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.struts;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.FieldExpressionValidator;
import com.opensymphony.xwork2.validator.annotations.RegexFieldValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.Result;

/**
 *
 * @author mickw
 */
public class RegisterAction extends ActionSupport {

    private String username, password, confirm, emailID, phoneNo;

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirm() {
        return confirm;
    }

    public String getEmailID() {
        return emailID;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    @Validations(
            requiredStrings = {
                @RequiredStringValidator(type = ValidatorType.FIELD, message = "Username is required")

            },
            stringLengthFields = {
                @StringLengthFieldValidator(type = ValidatorType.FIELD,
                        minLength = "6",
                        maxLength = "20",
                        message = "Username must be 6 to 20 chars")
            }
    )
    public void setUsername(String username) {
        this.username = username;
    }

    @Validations(
            requiredStrings = {
                @RequiredStringValidator(type = ValidatorType.FIELD, message = "Password is required")

            },
            stringLengthFields = {
                @StringLengthFieldValidator(type = ValidatorType.FIELD,
                        minLength = "6",
                        maxLength = "20",
                        message = "Password must be 6 to 20 chars")
            }
    )
    public void setPassword(String password) {
        this.password = password;
    }

    @FieldExpressionValidator(expression = "confirm==password",
            message = "Confirm must match Password")
    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    @EmailValidator(type = ValidatorType.FIELD, message = "email is invalid")
    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    @RegexFieldValidator(type = ValidatorType.FIELD, message = "phone number is invalid",
            regex = "\\d{3}-\\d{7}")
    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public RegisterAction() {
    }

    public String execute() throws Exception {
        System.out.println("exe..........");
        return "success";
    }

    @Action(value = "/register",
            exceptionMappings = {
                @ExceptionMapping(exception = "java.sql.SQLException", result = "input")
            }, results = {
                @Result(name = "success", location = "/index.jsp")
                ,
                @Result(name = "input", location = "/index.jsp")
            })
    public String insert() throws Exception {
        System.out.println("Insert.......");
        return "success";
    }
}
