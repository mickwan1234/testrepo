<%-- 
    Document   : insert
    Created on : Mar 7, 2019, 12:46:32 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="s" uri="/struts-tags" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <s:head/>
    </head>
    <body>
        <h1>Create account</h1>
        <s:form action="register">
            <s:textfield name="username" label="Username"/>
            <s:password name="password" label="Password"/>
            <s:password name="confirm" label="Confirm"/>
            <s:textfield name="emailId" label="Email"/>
            <s:textfield name="phoneNo" label="phoneNo"/>
            <s:submit value="Create new account"/>
        </s:form>
        <s:if test="%{exception.message.indexOf('duplicate') > -1}">
            <s:property value="username"/> is existed
        </s:if>
    </body>
</html>
