<%-- 
    Document   : insert
    Created on : Feb 14, 2019, 1:35:05 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Font Icon -->
        <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

        <!-- Main css -->
        <link rel="stylesheet" href="CSS/styleRegister.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="main">

            <section class="signup">
                <!-- <img src="images/signup-bg.jpg" alt=""> -->
                <div class="container">
                    <div class="signup-content">
                        <h2 class="form-title">Create account</h2>
                        <form action="MainController" method="POST" id="signup-form" class="signup-form" >
                            <div class="form-group">
                                <input type="text" class="form-input" id="name" placeholder="Username" name="txtUsername" value="${param.username}"/>
                                <font color="red">
                                ${requestScope.INVALID.usernameError}
                                </font>
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-input" id="password" placeholder="Password" name="txtPassword"/>
                                <font color="red">
                                ${requestScope.INVALID.passwordError}
                                </font>
                            </div>

                            <div class="form-group">
                                <input type="password" class="form-input" id="re_password" placeholder="Repeat your password" name="txtConfirm"/>
                                <font color="red">
                                ${requestScope.INVALID.confirmError}
                                </font>
                            </div>

                            <div class="form-group">
                                <input type="text"  class="form-input" id="name" placeholder="Fullname" name="txtFullname" value="${param.fullname}"/>
                                <font color="red">
                                ${requestScope.INVALID.fullnameError}
                                </font>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-input" name="txtEmail" id="email" placeholder="Your Email"/>
                                <font color="red">
                                ${requestScope.INVALID.emailError}
                                </font>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="PhoneNo" class="form-input" name="txtPhoneNo"/>
                                <font color="red">
                                ${requestScope.INVALID.phoneNoError}
                                </font>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="USER" value="${sessionScope.USER}"/>
                                <input type="submit" id="submit" class="form-submit" name="action" value="Insert"/>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="mb-5">
                            <h3 class="footer-heading mb-4">About Travelers</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
                        </div>



                    </div>
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <div class="row mb-5">
                            <div class="col-md-12">
                                <h3 class="footer-heading mb-4">Navigations</h3>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Destination</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">About</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Discounts</a></li>
                                </ul>
                            </div>
                        </div>



                    </div>

                    <div class="col-lg-4 mb-5 mb-lg-0">


                        <div class="mb-5">
                            <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

                            <form action="#" method="post">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary text-white" type="button" id="button-addon2">Send</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>

                </div>
                <div class="row pt-5 mt-5 text-center">
                    <div class="col-md-12">
                        <div class="mb-5">
                            <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                        </div>

                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>

                </div>
            </div>
        </footer>

        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-migrate-3.0.1.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/bootstrap-datepicker.min.js"></script>
        <script src="js/aos.js"></script>

        <script src="js/main.js"></script>
        <!-- JS -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="js/mainRegister.js"></script>
    </body>
</html>
