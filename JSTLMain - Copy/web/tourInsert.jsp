
<%@page import="java.util.Calendar"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Font Icon -->
        <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

        <!-- Main css -->
        <link rel="stylesheet" href="CSS/styleRegister.css">
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body style="background-image: url('http://i.imgur.com/qS7UAgb.jpg')">
        <c:if test="${sessionScope.USER.role == null}">
            <c:redirect url="index.jsp"/>
        </c:if>
        <div class="main">
            <section class="signup">
                <!-- <img src="images/signup-bg.jpg" alt=""> -->
                <div class="container">
                    <div class="signup-content">
                        <h2 class="form-title">Insert Tour</h2>
                        <form action="TourMainController" method="POST" id="signup-form" class="signup-form">
                            <div class="form-group">
                                <input type="text"  placeholder="Tour ID" class="form-input" id="name" name="txtID"/> <font color="red">${requestScope.INVALID.idError}</font><br/>
                            </div>
                            <div class="form-group">
                                <input type ="text" placeholder="Tour Name" class="form-input" id="name" name="txtName"/><font color="red">${requestScope.INVALID.nameError}</font><br/>
                            </div>
                            <div class="form-group">
                                 <textarea placeholder="Description" class="form-input" id="name" name="txtDescription" value="${entry.description}"></textarea><font color="red">${requestScope.INVALID.descriptionError}</font><br/>
                            </div>
                            <font style="color: grey">Time:</font>
                            <div class="form-group">
                              <input  type="date" class="form-control" name="cblDate"/>
                                <font color="red">${requestScope.INVALID.timeError}</font><br/>
                            </div>
                            <font style="color: grey">Time End: </font>
                            <div class="form-group">
                               <input class="form-control" type="date" name="cblEndDate"/>
                                <font color="red">${requestScope.INVALID.timeError}</font><br/>
                            </div>
                            <div class="form-group">
                               <input type="text" placeholder="Price" class="form-input" id="name" name="txtPrice"/><font color="red">${requestScope.INVALID.priceError}${requestScope.INVALID.numberError}</font><br/>
                            </div>
                            <div class="form-group">
                               <input type="text" placeholder="Image link" class="form-input" id="name" name="txtImageLink"/><font color="red">${requestScope.INVALID.imageLinkError}</font><br/>
                            </div>
                            <div class="form-group">
                                <select name="cblRegionID" class="form-control">
                                    <option>Eu (Europe)</option>
                                    <option>As (Asia)</option>
                                    <option>Af (Africa)</option>
                                    <option>SA (South America)</option>
                                    <option>Aus (Australia)</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Insert Tour" id="submit" class="form-submit" name="action"/>${requestScope.INVALID.error}
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <!-- JS -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="js/mainRegister.js"></script>
    </body>
</html>
