<%-- 
    Document   : detailPage
    Created on : Mar 17, 2019, 7:43:17 PM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
        <link rel="stylesheet" href="fonts/icomoon/style.css">
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" href="CSS/bootstrap.min.css">
        <link rel="stylesheet" href="CSS/magnific-popup.css">
        <link rel="stylesheet" href="CSS/jquery-ui.css">
        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/owl.theme.default.min.css">

        <link rel="stylesheet" href="CSS/bootstrap-datepicker.css">

        <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">


        <link rel="stylesheet" href="CSS/aos.css">

        <link rel="stylesheet" href="CSS/style.css">

        <link rel="stylesheet" type="text/css" href="CSS/searchBar.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${sessionScope.USER.role == null}">
            <c:redirect url="index.jsp"/>
        </c:if>
        <div class="container">
            <div class="row pt-1 pb-1">
                <div class="col-lg-12">
                    <center> <h1>Welcome to Book tour Page ${sessionScope.USER.fullname}</h1></center>
                </div>
            </div>
        </div>
        <header class="site-navbar py-1" role="banner">

            <div class="container">
                <div class="row align-items-center">

                    <div class="col-6 col-xl-2">
                        <h1 class="mb-0"><a href="index.html" class="text-black h2 mb-0">Travelers</a></h1>
                    </div>
                    <div class="col-10 col-md-8 d-none d-xl-block">
                        <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

                            <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                                <li class="active">
                                    <form action="view.jsp" method="POST">
                                        <input type="submit" class="btn btn-outline-dark" value="View Cart"/>
                                    </form>
                                </li>
                                <li class="has-children">
                                    <form action="TourMainController" method="POST">
                                        <input type="submit" class="btn btn-outline-dark" name="action" value="View Bill"/>
                                    </form>
                                </li>
                                <li>            
                                    <form action="MainController" method="POST">
                                        <input type="hidden" name="username" value="${sessionScope.USER.username}"/>
                                        <input type="submit" class="btn btn-outline-dark" name="action" value="Edit"/>
                                    </form>
                                </li>
                                <li>            
                                    <form action="MainController" method="POST">
                                        <input type="submit" class="btn btn-outline-dark" name="action" value="Logout"/>
                                    </form>
                                </li>
                                <!-- <li><a href="booking.html">Book Online</a></li> -->
                            </ul>
                        </nav>
                    </div>

                    <div class="col-6 col-xl-2 text-right">
                        <div class="d-none d-xl-inline-block">
                            <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                                <li>
                                    <a href="#" class="pl-0 pr-3 text-black"><span class="icon-tripadvisor"></span></a>
                                </li>
                                <li>
                                    <a href="#" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                                </li>
                                <li>
                                    <a href="#" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                                </li>
                                <li>
                                    <a href="#" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                                </li>

                            </ul>
                        </div>

                        <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

                    </div>

                </div>
            </div>
        </header>
        <section >
            <div style="position:relative">
                <section >
                    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
                        <div class="carousel-inner">
                            <div class="carousel-item active">
                                <img src="${requestScope.INFO.image}" class="d-block w-100" alt="...">
                            </div>
                        </div>
                    </div>
                </section>
                <br/>

                <!-- Page Content -->
                <div class="container">

                    <!-- Portfolio Item Heading -->
                    <h1 class="my-4">${requestScope.INFO.name}
                    </h1>

                    <!-- Portfolio Item Row -->
                    <div class="row">

                        <div class="col-md-8">
                            <img class="img-fluid" src="${requestScope.INFO.image}" alt="">
                        </div>

                        <div class="col-md-4">
                            <h3 class="my-3">Tour Description</h3>
                            <p>${requestScope.INFO.description}</p>
                            <h3 class="my-3">Tour Details</h3>
                            <ul>

                                <li>Start date ${requestScope.INFO.time}</li>
                                <li>End date: ${requestScope.INFO.endTime}</li>
                                <li>Price: ${requestScope.INFO.price} / person</li>
                                    <c:if test="${sessionScope.USER.role == 'User'}">
                                    <li>
                                        <form action="TourMainController" method="POST">
                                            <input type="hidden" value="${requestScope.INFO.id}" name="txtID"/>
                                            <input type="hidden" value="${param.txtSearch}"/>
                                            <input type="hidden" name="txtName" value="${requestScope.INFO.name}"/>
                                            <input type="hidden" name="txtPrice" value="${requestScope.INFO.price}"/>
                                            <input type="hidden" name="txtTime" value="${requestScope.INFO.time}"/>
                                            <input type="hidden" name="txtEndTime" value="${requestScope.INFO.endTime}"/>
                                            <input type="hidden" name="txtImage" value="${requestScope.INFO.image}"/>
                                            <input type="hidden" name="txtQuantity" value="${sessionScope.cblRegion}"/>
                                            <input type="hidden" name="txtRegionID" value="${requestScope.INFO.regionID}"/>
                                            <input type="hidden" name="txtDescription" value="${requestScope.INFO.description}"/>
                                            <input type="submit" name="action" class="btn btn-outline-primary" value="Add" style="align-self: center"/>
                                        </form></li>
                                    </c:if>
                                    <c:if test="${sessionScope.USER.role == 'Admin'}">
                                    <li>
                                        <form action="updateTour.jsp" method="POST">
                                            <input type="hidden" value="${requestScope.INFO.id}" name="txtID"/>
                                            <input type="hidden" value="${param.txtSearch}"/>
                                            <input type="hidden" name="txtName" value="${requestScope.INFO.name}"/>
                                            <input type="hidden" name="txtPrice" value="${requestScope.INFO.price}"/>
                                            <input type="hidden" name="txtTime" value="${requestScope.INFO.time}"/>
                                            <input type="hidden" name="txtEndTime" value="${requestScope.INFO.endTime}"/>
                                            <input type="hidden" name="txtImage" value="${requestScope.INFO.image}"/>
                                            <input type="hidden" name="txtQuantity" value="${sessionScope.cblRegion}"/>
                                            <input type="hidden" name="txtRegionID" value="${requestScope.INFO.regionID}"/>
                                            <input type="hidden" name="cblRegion" value="${param.cblRegion}"/>
                                            <input type="hidden" name="txtDescription" value="${requestScope.INFO.description}"/>
                                            <input type="submit" class="btn btn-outline-primary" name="action" value="Update Tour"/>
                                        </form>
                                    </li>
                                    <li>
                                        <form action="TourMainController" method="POST">
                                            <input type="hidden" name="txtSearch"  value="${param.txtSearch}"/>
                                            <input type="hidden" value="${requestScope.INFO.id}" name="txtID"/>
                                            <input type="submit" class="btn btn-outline-primary" name="action" value="Delete Tour"/>
                                        </form>
                                    </li>
                                </c:if>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.row -->
                <footer class="site-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="mb-5">
                                    <h3 class="footer-heading mb-4">About Travelers</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
                                </div>



                            </div>
                            <div class="col-lg-4 mb-5 mb-lg-0">
                                <div class="row mb-5">
                                    <div class="col-md-12">
                                        <h3 class="footer-heading mb-4">Navigations</h3>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <ul class="list-unstyled">
                                            <li><a href="#">Home</a></li>
                                            <li><a href="#">Destination</a></li>
                                            <li><a href="#">Services</a></li>
                                            <li><a href="#">About</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <ul class="list-unstyled">
                                            <li><a href="#">About Us</a></li>
                                            <li><a href="#">Privacy Policy</a></li>
                                            <li><a href="#">Contact Us</a></li>
                                            <li><a href="#">Discounts</a></li>
                                        </ul>
                                    </div>
                                </div>



                            </div>
                        </div>

                        <div class="col-lg-4 mb-5 mb-lg-0">

                            <div class="mb-5">
                                <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

                                <form action="#" method="post">
                                    <div class="input-group mb-3">
                                        <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary text-white" type="button" id="button-addon2">Send</button>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>

                    </div>
                    <div class="row pt-5 mt-5 text-center">
                        <div class="col-md-12">
                            <div class="mb-5">
                                <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                                <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                                <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                                <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                            </div>

                            <p>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>

                    </div>

                </footer>

                <script src="js/jquery-3.3.1.min.js"></script>
                <script src="js/jquery-migrate-3.0.1.min.js"></script>
                <script src="js/jquery-ui.js"></script>
                <script src="js/popper.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/owl.carousel.min.js"></script>
                <script src="js/jquery.stellar.min.js"></script>
                <script src="js/jquery.countdown.min.js"></script>
                <script src="js/jquery.magnific-popup.min.js"></script>
                <script src="js/bootstrap-datepicker.min.js"></script>
                <script src="js/aos.js"></script>
                <script src="js/main.js"></script>
                </body>
                </html>
