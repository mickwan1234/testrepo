/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import nghia.connection.MyConnection;
import nghia.dtos.RegistrationDTO;

/**
 *
 * @author mickw
 */
public class RegistrationDAO implements Serializable {

    private Connection conn = null;
    private PreparedStatement preStm = null;
    private ResultSet rs = null;

    private void closeConection() throws Exception {
        if (rs != null) {
            rs.close();
        }
        if (preStm != null) {
            preStm.close();
        }
        if (conn != null) {
            conn.close();
        }

    }

    public RegistrationDTO checkLogin(String username, String password) throws Exception {
        RegistrationDTO dto = null;
        String fullname, role, phoneNo, email;
        try {
            String sql = "Select * from Registration where Username = ? and Password = ?";
            conn = MyConnection.myConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, username);
            preStm.setString(2, password);
            rs = preStm.executeQuery();
            if (rs.next()) {
                fullname = rs.getString("Fullname");
                phoneNo = rs.getString("PhoneNo");
                email = rs.getString("Email");
                role = rs.getString("Role");
                dto = new RegistrationDTO(username, password, fullname, role, phoneNo, email);
            }
        } finally {
            closeConection();
        }
        return dto;
    }

    public List<RegistrationDTO> findByLikeName(String search) throws Exception {
        List<RegistrationDTO> result = new ArrayList<>();
        String username = null;
        String fullname = null;
        String phoneNo = null;
        String email = null;
        String role = null;
        RegistrationDTO dto = null;
        try {
            String sql = "Select Username, Fullname, Role, phoneNo, Email from Registration where Fullname like ? and isDelete = 0";
            conn = MyConnection.myConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, "%" + search + "%");
            rs = preStm.executeQuery();
            while (rs.next()) {
                username = rs.getString("Username");
                fullname = rs.getString("Fullname");
                role = rs.getString("Role");
                phoneNo = rs.getString("phoneNo");
                email = rs.getString("Email");
                dto = new RegistrationDTO(username, null, fullname, role, phoneNo, email);
                System.out.println(dto);
                result.add(dto);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConection();

        }
        System.out.println(result);
        return result;
    }
// not done

    public boolean delete(String id) throws Exception {
        boolean check = false;
        try {
            String sql = "Update Registration set IsDelete = 1 where username = ?";
            conn = MyConnection.myConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, id);
            check = preStm.executeUpdate() > 0;
        } finally {
            closeConection();
        }
        return check;
    }
// end

    public RegistrationDTO finbyPrimaryKey(String key) throws Exception {
        RegistrationDTO dto = null;
        try {
            String sql = "Select fullname, role, phoneNo, Email from Registration Where username = ?";
            conn = MyConnection.myConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, key);
            rs = preStm.executeQuery();
            if (rs.next()) {
                String fullname = rs.getString("Fullname");
                String role = rs.getString("Role");
                String phoneNo = rs.getString("PhoneNo");
                String email = rs.getString("email");
                dto = new RegistrationDTO(key, null, fullname, role, phoneNo, email);
            }
        } finally {
            closeConection();
        }
        return dto;
    }
      public List<RegistrationDTO> finbyPrimaryKeyList(String key) throws Exception {
        List<RegistrationDTO> dtos = new ArrayList<>();
        try {
            String sql = "Select username, fullname, role, phoneNo, Email from Registration Where username like ? and isDelete = 0";
            conn = MyConnection.myConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1,"%"+key+"%");
            rs = preStm.executeQuery();
            while(rs.next()) {
                String fullname = rs.getString("Fullname");
                String role = rs.getString("Role");
                String phoneNo = rs.getString("PhoneNo");
                String email = rs.getString("email");
                String username = rs.getString("Username");
               RegistrationDTO dto = new RegistrationDTO(username, null, fullname, role, phoneNo, email);
               dtos.add(dto);
            }
        } finally {
            closeConection();
        }
        return dtos;
    }

    public boolean update(RegistrationDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Update Registration set Fullname = ?,  PhoneNo=?, Email=?  where Username = ?";
            conn = MyConnection.myConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, dto.getFullname());
            preStm.setString(2, dto.getPhoneNo());
            preStm.setString(3, dto.getEmail());
            preStm.setString(4, dto.getUsername());
            check = preStm.executeUpdate() > 0;
            System.out.println(dto);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            closeConection();
        }
        return check;
    }

    public boolean insert(RegistrationDTO dto) throws Exception {
        boolean check = false;
        try {
            String sql = "Insert into Registration(Username,Password,Fullname,Role, Email, PhoneNo,isDelete) values(?,?,?,?,?,?,0)";
            conn = MyConnection.myConnection();
            preStm = conn.prepareStatement(sql);
            preStm.setString(1, dto.getUsername());
            preStm.setString(2, dto.getPassword());
            preStm.setString(3, dto.getFullname());
            preStm.setString(4, dto.getRole());
            preStm.setString(5, dto.getEmail());
            preStm.setString(6, dto.getPhoneNo());
            check = preStm.executeUpdate() > 0;
        } finally {
            closeConection();
        }
        return check;
    }
}
