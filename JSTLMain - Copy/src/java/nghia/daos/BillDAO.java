/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;
import nghia.connection.MyConnection;
import nghia.dtos.BillDTO;
import nghia.dtos.CartDTO;

/**
 *
 * @author mickw
 */
public class BillDAO implements Serializable {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    private void closeConnection() throws Exception {
        if (conn != null) {
            conn.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public boolean insert(CartDTO dto, String tourID) throws Exception {
        boolean check = false;
        try {
            String sql = "Insert into Bill (Username, tourID, Quantity, TotalPrice,Date) values(?,?,?,?,?)";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getUsername());
            ps.setString(2, dto.getBill().get(tourID).getId());
            ps.setInt(3, dto.getBill().get(tourID).getQuantity());
            ps.setFloat(4, dto.getBill().get(tourID).getPrice() * dto.getBill().get(tourID).getQuantity());
            ps.setDate(5, new Date(Calendar.getInstance().getTime().getTime()));
            check = ps.executeUpdate() > 0;
        } finally {
            closeConnection();
        }
        return check;
    }

    public ArrayList<BillDTO> findByUsername(String username) throws Exception {
        ArrayList<BillDTO> dtos = new ArrayList<>();
        try {
            String sql = "use ProjectTour\n"
                    + "SELECT b.ID, b.Username,b.TourID,b.Quantity,b.TotalPrice,b.Date, Tour.Name\n"
                    + "FROM Bill b\n"
                    + "Inner join Tour on Tour.ID = b.TourID\n"
                    + "where Username = ?;";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("ID");
                String tourId = rs.getString("TourID");
                int quantity = rs.getInt("Quantity");
                float totalPrice = rs.getFloat("totalPrice");
                Date date = rs.getDate("Date");
                String tourName = rs.getString("Name");
                BillDTO dto = new BillDTO(username, tourId, tourName, totalPrice, date, id, quantity);
                dtos.add(dto);
            }
        } finally {
            closeConnection();
        }
        return dtos;
    }
}
