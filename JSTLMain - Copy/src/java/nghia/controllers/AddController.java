/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nghia.dtos.CartDTO;
import nghia.dtos.RegistrationDTO;
import nghia.dtos.TourDTO;

/**
 *
 * @author mickw
 */
public class AddController extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            HttpSession session = request.getSession();
            RegistrationDTO dto = (RegistrationDTO) session.getAttribute("USER");
            session.setAttribute("cblRegion", session.getAttribute("cblRegion"));
            CartDTO cart = (CartDTO) session.getAttribute("shoppingCart");
            if (cart == null) {
                cart = new CartDTO(dto.getUsername());
            }
            //od-name-price
            TourDTO tour = new TourDTO();
            tour.setId(request.getParameter("txtID"));
            tour.setName(request.getParameter("txtName"));
            tour.setPrice(Float.parseFloat(request.getParameter("txtPrice")));
            tour.setQuantity(1);
            tour.setRegionID(request.getParameter("RegionID"));
            tour.setImage(request.getParameter("txtImage"));
            tour.setTime( new java.sql.Date(new SimpleDateFormat("YYYY-MM-DD").parse(request.getParameter("txtTime")).getTime()));
            tour.setEndTime(new java.sql.Date(new SimpleDateFormat("YYYY-MM-DD").parse(request.getParameter("txtEndTime")).getTime()));
            cart.addToCart(tour);
            session.setAttribute("shoppingCart", cart);
        } catch (Exception e) {
            e.printStackTrace();
            log("ERROR at AddController: " + e.getMessage());
        } finally {
            request.getRequestDispatcher("ViewController").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
