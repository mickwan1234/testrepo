/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nghia.dtos.CartDTO;
import nghia.dtos.TourDTO;
import nghia.dtos.TourErrorObject;

/**
 *
 * @author mickw
 */
public class CartUpdateController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        TourErrorObject errorObj = new TourErrorObject();
        try {
            String[] listID = request.getParameterValues("txtID");
            String[] listQuantity = request.getParameterValues("txtQuantity");
            HttpSession session = request.getSession();
            CartDTO cart = (CartDTO) session.getAttribute("shoppingCart");
            boolean valid = true;
            for (int i = 0; i < listQuantity.length; i++) {
                if (listQuantity[i].trim().length() == 0) {
                    errorObj.setQuantityError("Quantity must not be blank");
                    valid = false;
                } else if (Integer.parseInt(listQuantity[i]) <= 0) {
                    errorObj.setQuantityError("Quantity must greater than 0");
                    valid = false;
                }

            }
            if (valid) {
                for (int i = 0; i < listID.length; i++) {
                    cart.updateCart(listID[i], Integer.parseInt(listQuantity[i]));
                }
                session.setAttribute("shoppingCart", cart);
            } else {
                request.setAttribute("INVALID", errorObj);
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (e.getMessage().contains("For input string: \"\"")) {
                errorObj.setQuantityError("Not a number");
                request.setAttribute("INVALID", errorObj);
            }
            if (e.toString().contains("NumberFormat")) {
                errorObj.setQuantityError("Not a number");
                request.setAttribute("INVALID", errorObj);
            }
            log("Error at Cart update Controller: " + e.getMessage());
        } finally {
            request.getRequestDispatcher("view.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
