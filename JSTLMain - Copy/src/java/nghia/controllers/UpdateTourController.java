/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.StringTokenizer;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import nghia.daos.TourDAO;
import nghia.dtos.RegistrationDTO;
import nghia.dtos.TourDTO;
import nghia.dtos.TourErrorObject;

/**
 *
 * @author mickw
 */
public class UpdateTourController extends HttpServlet {

    private static final String ERROR = "error.jsp";
    private static final String INVALID = "updateTour.jsp";
    private static final String SUCCESS = "TourViewController";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        StringTokenizer stk = new StringTokenizer(request.getParameter("cblRegionID"), "(");
        TourErrorObject errorObj = new TourErrorObject();
        try {
            HttpSession session = request.getSession();
            RegistrationDTO dtoUser = (RegistrationDTO) session.getAttribute("USER");
            session.setAttribute("USER", dtoUser);
            String id = request.getParameter("txtID");
            String name = request.getParameter("txtName");
            String image = request.getParameter("txtImageLink");
            String timeString = request.getParameter("txtTime");
            java.util.Date date = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("cblTime"));
             System.out.println(request.getParameter("cblTime"));
            Calendar c = Calendar.getInstance();
            c.setTime(date);
            date = c.getTime();
            Date time = new java.sql.Date(date.getTime());
            System.out.println(request.getParameter("cblEndTime"));
            java.util.Date endDate = new SimpleDateFormat("yyyy-MM-dd").parse(request.getParameter("cblEndTime"));
            Calendar cEnd = Calendar.getInstance();
            cEnd.setTime(endDate);
            endDate = cEnd.getTime();
            Date timeEnd = new java.sql.Date(endDate.getTime());
            System.out.println(timeEnd);
            float price = Float.parseFloat(request.getParameter("txtPrice"));
            String regionID = stk.nextToken();
            String description = request.getParameter("txtDescription");
            TourDTO dto = new TourDTO(id, name, image, regionID, description, time, timeEnd, false, 0, price);
            boolean valid = true;
            if (id.length() == 0) {
                errorObj.setIdError("Id cannot be blank");
                valid = false;
            }
            if (name.length() == 0) {
                errorObj.setNameError("Name cannot be blank");
                valid = false;
            }
            if (image.length() == 0) {
                errorObj.setImageLinkError("Image link cannot be blank");
                valid = false;
            }
            if (price <= 0) {
                errorObj.setPriceError("Price cannot smaller than 0");
                valid = false;
            }
            if (description.length() == 0) {
                errorObj.setDescriptionError("Description cannot be blank");
                valid = false;
            }
            if (valid) {
                TourDAO dao = new TourDAO();
                if (dao.update(dto)) {
                    url = SUCCESS;
                } else {
                    request.setAttribute("ERROR", "Update tour failed");
                }
            } else {
                request.setAttribute("INVALID", errorObj);
                url = INVALID;
            }
        } catch (Exception e) {
            if (e.toString().contains("NumberFormat")) {
                errorObj.setNumberError("Number format error");
                url = INVALID;
            }
            if (e.toString().contains("Parse")) {
                errorObj.setTimeError("date is wrong");
                url = INVALID;
            } else {
                errorObj.setError(e.toString());
                request.setAttribute("INVALID", errorObj);
                url = INVALID;
            }
            if (e.toString().contains("duplicate")) {
                errorObj.setIdError("Id already existed");
                request.setAttribute("INVALID", errorObj);
                url = INVALID;
            }
            log("Error at InsertTourController: " + e.toString());
            e.printStackTrace();
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
