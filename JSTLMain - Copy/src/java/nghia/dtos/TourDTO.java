/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.dtos;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author mickw
 */
public class TourDTO implements Serializable {

    String id, name, image, regionID, description;

    public TourDTO(String id, String name, String image, String regionID, String description, Date time, Date endTime, boolean isDelete, int quantity, float price) {
        this.id = id;
        this.name = name;
        this.image = image;
        this.regionID = regionID;
        this.description = description;
        this.time = time;
        this.endTime = endTime;
        this.isDelete = isDelete;
        this.quantity = quantity;
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    Date time, endTime;
    boolean isDelete;


    public boolean isIsDelete() {
        return isDelete;
    }

    public void setIsDelete(boolean isDelete) {
        this.isDelete = isDelete;
    }


    public TourDTO() {
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
    int quantity;
    float price;

    public void setRegionID(String regionID) {
        this.regionID = regionID;
    }

    public String getRegionID() {
        return regionID;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Date getTime() {
        return time;
    }


    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "TourDTO{" + "id=" + id + ", name=" + name + ", image=" + image + ", regionID=" + regionID + ", description=" + description + ", time=" + time + ", endTime=" + endTime + ", isDelete=" + isDelete + ", quantity=" + quantity + ", price=" + price + '}';
    }



}
