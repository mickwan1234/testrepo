/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.dtos;

import java.io.Serializable;
import java.sql.Date;

/**
 *
 * @author mickw
 */
public class BillDTO implements Serializable {

    private String username, tourId, tourName;
    private float totalPrice;
    private Date date;
    private int id, quantity;

    public BillDTO(String username, String tourId, String tourName, float totalPrice, Date date, int id, int quantity) {
        this.username = username;
        this.tourId = tourId;
        this.tourName = tourName;
        this.totalPrice = totalPrice;
        this.date = date;
        this.id = id;
        this.quantity = quantity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getTourId() {
        return tourId;
    }

    public void setTourId(String tourId) {
        this.tourId = tourId;
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public float getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(float totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "BillDTO{" + "id=" + id + ", username=" + username + ", tourId=" + tourId + ", tourName=" + tourName + ", totalPrice=" + totalPrice + ", date=" + date + '}';
    }

}
