/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.dtos;

import java.io.Serializable;
import java.util.HashMap;

/**
 *
 * @author mickw
 */
public class CartDTO implements Serializable {

    String username;
    HashMap<String, TourDTO> bill;
    int quantity;
    public CartDTO() {
        username = "Guest";
        bill = new HashMap<>();
    }

    public CartDTO(String username) {
        this.username = username;
        bill = new HashMap<>();
    }

    public String getUsername() {
        return username;
    }

    public HashMap<String, TourDTO> getBill() {
        return bill;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setBill(HashMap<String, TourDTO> bill) {
        this.bill = bill;
    }

    public void addToCart(TourDTO dto) throws Exception {
        if (this.bill.containsKey(dto.getId())) {
            int quantity = this.bill.get(dto.getId()).getQuantity() + 1;
            dto.setQuantity(quantity);
        }
        this.bill.put(dto.getId(), dto);
        System.out.println(this.bill);
    }

    public boolean removeCart(String id) throws Exception {
        boolean check = true;
        if (this.bill.containsKey(id)) {
            this.bill.remove(id);
        } else {
            check = false;
        }
        return check;
    }

    public boolean updateCart(String id, int quantity) throws Exception {
        boolean check = true;
        if(this.bill.containsKey(id)){
            this.bill.get(id).setQuantity(quantity);
        }else{
            check = false;
        }
        return check;
    }
     public float getTotal() throws Exception{
         float total = 0;
         for(TourDTO dto : this.bill.values()){
             total += dto.getQuantity() * dto.getPrice();
         }
         return total;
     }
     
     public int getQuantity(String id) throws Exception{
         int quantity = 0;
         return quantity;
     }

    @Override
    public String toString() {
        return "BillDTO{" + "username=" + username + ", bill=" + bill + ", quantity=" + quantity + '}';
    }
     
     public CartDTO getReceipt(){
         return this;
     }
}
