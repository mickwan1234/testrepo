/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.dtos;

import java.io.Serializable;

/**
 *
 * @author mickw
 */
public class RegistrationErrorObject implements Serializable {

    private String usernameError, passwordError, fullnameError, roleError, confirmError, phoneNoError, emailError;

    public String getPhoneNoError() {
        return phoneNoError;
    }

    public String getEmailError() {
        return emailError;
    }

    public void setPhoneNoError(String phoneNoError) {
        this.phoneNoError = phoneNoError;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getConfirmError() {
        return confirmError;
    }

    public void setConfirmError(String confirmError) {
        this.confirmError = confirmError;
    }

    public String getFullnameError() {
        return fullnameError;
    }

    public String getRoleError() {
        return roleError;
    }

    public void setFullnameError(String fullnameError) {
        this.fullnameError = fullnameError;
    }

    public void setRoleError(String roleError) {
        this.roleError = roleError;
    }

    public RegistrationErrorObject() {
    }

    public String getUsernameError() {
        return usernameError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    public void setUsernameError(String usernameERROR) {
        this.usernameError = usernameERROR;
    }

    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
    }

}
