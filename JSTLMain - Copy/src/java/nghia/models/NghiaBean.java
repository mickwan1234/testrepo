/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.models;

import java.io.Serializable;
import java.util.List;
import dao.RegistrationDAO;
import nghia.dtos.RegistrationDTO;

/**
 *
 * @author mickw
 */
public class NghiaBean implements Serializable {

    private String username, password, search;
    private RegistrationDTO dto;

    public void setDto(RegistrationDTO dto) {
        this.dto = dto;
    }

    public RegistrationDTO getDto() {
        return dto;
    }

    public NghiaBean() {
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RegistrationDTO checkLogin() throws Exception {
        RegistrationDAO dao = new RegistrationDAO();
        return dao.checkLogin(username, password);
    }

    public List<RegistrationDTO> findByLikeName() throws Exception {
        RegistrationDAO dao = new RegistrationDAO();
        return dao.findByLikeName(search);
    }

    public boolean delete() throws Exception {
        RegistrationDAO dao = new RegistrationDAO();
        return dao.delete(username);
    }

    public RegistrationDTO findByPrimaryKey() throws Exception {
        RegistrationDAO dao = new RegistrationDAO();
        return dao.finbyPrimaryKey(username);
    }

    public boolean update() throws Exception {
        RegistrationDAO dao = new RegistrationDAO();
        return dao.update(dto);
    }

    public boolean insert() throws Exception {
        RegistrationDAO dao = new RegistrationDAO();
        return dao.insert(dto);
    }

    public List<RegistrationDTO> findByPrimaryKeyList() throws Exception{
    RegistrationDAO dao = new RegistrationDAO();
    return dao.finbyPrimaryKeyList(username);
}
    @Override
    public String toString() {
        return "NghiaBean{" + "username=" + username + ", password=" + password + ", search=" + search + '}';
    }

}
