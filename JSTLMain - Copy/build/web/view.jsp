<%-- 
    Document   : view
    Created on : Mar 4, 2019, 2:50:39 PM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
        <link rel="stylesheet" href="fonts/icomoon/style.css">
        <link rel="stylesheet" href="CSS/table.css">
        <link rel="stylesheet" href="CSS/bootstrap.min.css">
        <link rel="stylesheet" href="CSS/magnific-popup.css">
        <link rel="stylesheet" href="CSS/jquery-ui.css">
        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/owl.theme.default.min.css">

        <link rel="stylesheet" href="CSS/bootstrap-datepicker.css">

        <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">


        <link rel="stylesheet" href="CSS/aos.css">

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="CSS/table.css">
        <title>JSP Page</title>
    </head>
    <body>
        <c:if test="${sessionScope.USER.role == null}">
            <c:redirect url="index.jsp"/>
        </c:if>
        <h1>${sessionScope.USER.fullname}'s Cart</h1>

        <c:if test="${sessionScope.shoppingCart != null}">
            <c:if test="${not empty sessionScope.shoppingCart }" var="check">
                <form action="TourMainController" method="POST">
                    <table class="table table-hover shopping-cart-wrap" >
                        <thead class="text-muted" >
                            <tr>                           
                                <th scope="col" >Tour name</th>
                                <th scope="col" >Username</th>
                                <th scope="col" >TourID</th>
                                <th scope="col" >Quantity</th>
                                <th scope="col" >TotalPrice</th>
                                <th scope="col">Date</th>
                                <th scope="col" >Delete</th>
                                <th scope="col">Update</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="dto" items="${sessionScope.shoppingCart.bill}">
                                <tr>
                                    <td>
                                        <div class="img-wrap"><img src="${dto.value.image}" class="img-thumbnail img-sm"></div>
                            <figcaption class="media-body"> ${dto.value.name}</figcaption>
                            </td>
                            <td>${sessionScope.USER.username}</td>
                            <td> <input type="text" style="border: 0" value="${dto.value.id}" disabled="true"/> </td>
                            <td><input type="hidden" name="txtID" value="${dto.value.id}" />
                                <input type="text" name="txtQuantity" value="${dto.value.quantity}"/>
                                <font color="red">${requestScope.INVALID.quantityError}</font>
                            </td>
                            <td>${dto.value.price} for each person</td>
                            <td>${dto.value.time}</td>
                            <td>
                                <input type="hidden" name="cartElement" value="${dto.value.id}"/>
                                <input type="hidden" name="shoppingCart" value="${sessionScope.shoppingCart}"/>
                                <input type="submit" class="btn btn-danger" name="action" value="Delete"/>
                            </td>
                            <td>
                                <input type="hidden" name="cartElement" value="${dto.value.id}"/>
                                <input type="hidden" name="shoppingCart" value="${sessionScope.shoppingCart}"/>
                                <input type="submit" class="btn btn-success" name="action" value="Update"/></td>
                            </tr>
                        </c:forEach>
                        <tr>
                            <td>Total price : </td>
                            <td style="width: 100%">	
                                <div class="price-wrap"> 
                                    <var class="price">${shoppingCart.total}</var>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td
                                ><a href="user.jsp">Continue shopping</a></td>
                        </tr>
                        </tbody>
                    </table>
                    <input type="hidden" name="shoppingCart" value="${sessionScope.shoppingCart}"/>
                    <input type="submit" class="btn btn-primary btn-lg" style="float: right; margin-right: 10%" name="action" value="Book"/>
                </form>
            </c:if>
        </c:if>
        <c:if test="${sessionScope.shoppingCart == null}">
            <h2>Nothing in cart</h2>
        </c:if>
        <footer class="site-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4">
                        <div class="mb-5">
                            <h3 class="footer-heading mb-4">About Travelers</h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
                        </div>



                    </div>
                    <div class="col-lg-4 mb-5 mb-lg-0">
                        <div class="row mb-5">
                            <div class="col-md-12">
                                <h3 class="footer-heading mb-4">Navigations</h3>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#">Home</a></li>
                                    <li><a href="#">Destination</a></li>
                                    <li><a href="#">Services</a></li>
                                    <li><a href="#">About</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6 col-lg-6">
                                <ul class="list-unstyled">
                                    <li><a href="#">About Us</a></li>
                                    <li><a href="#">Privacy Policy</a></li>
                                    <li><a href="#">Contact Us</a></li>
                                    <li><a href="#">Discounts</a></li>
                                </ul>
                            </div>
                        </div>



                    </div>

                    <div class="col-lg-4 mb-5 mb-lg-0">


                        <div class="mb-5">
                            <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

                            <form action="#" method="post">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary text-white" type="button" id="button-addon2">Send</button>
                                    </div>
                                </div>
                            </form>

                        </div>

                    </div>

                </div>
                <div class="row pt-5 mt-5 text-center">
                    <div class="col-md-12">
                        <div class="mb-5">
                            <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                            <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                        </div>

                        <p>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>

                </div>
            </div>
        </footer>

        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/jquery-migrate-3.0.1.min.js"></script>
        <script src="js/jquery-ui.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/owl.carousel.min.js"></script>
        <script src="js/jquery.stellar.min.js"></script>
        <script src="js/jquery.countdown.min.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/bootstrap-datepicker.min.js"></script>
        <script src="js/aos.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>
