<%-- 
    Document   : admin
    Created on : Feb 12, 2019, 3:18:55 PM
    Author     : mickw
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,700,900|Display+Playfair:200,300,400,700"> 
        <link rel="stylesheet" href="fonts/icomoon/style.css">
        <link rel="stylesheet" href="CSS/table.css">
        <link rel="stylesheet" href="CSS/bootstrap.min.css">
        <link rel="stylesheet" href="CSS/magnific-popup.css">
        <link rel="stylesheet" href="CSS/jquery-ui.css">
        <link rel="stylesheet" href="CSS/owl.carousel.min.css">
        <link rel="stylesheet" href="CSS/owl.theme.default.min.css">

        <link rel="stylesheet" href="CSS/bootstrap-datepicker.css">

        <link rel="stylesheet" href="fonts/flaticon/font/flaticon.css">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/mediaelement@4.2.7/build/mediaelementplayer.min.css">


        <link rel="stylesheet" href="CSS/aos.css">

        <link rel="stylesheet" href="CSS/style.css">

        <link rel="stylesheet" type="text/css" href="CSS/searchBar.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Admin Manage Page</title>
    </head>
    <body>
        <c:if test="${sessionScope.USER.role == null}">
            <c:redirect url="index.jsp"/>
        </c:if>
        <div class="container">
            <div class="row pt-1 pb-1">
                <div class="col-lg-12">
                    <center> <h1>Welcome to Admin Page ${sessionScope.USER.fullname}</h1></center>
                </div>
            </div>
        </div>
        <header class="site-navbar py-1" role="banner">

            <div class="container">
                <div class="row align-items-center">

                    <div class="col-6 col-xl-2">
                        <h1 class="mb-0"><a href="index.html" class="text-black h2 mb-0">Travelers</a></h1>
                    </div>
                    <div class="col-10 col-md-8 d-none d-xl-block">
                        <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

                            <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                                <li>            
                                    <form action="MainController" method="POST">
                                        <input type="hidden" name="username" value="${sessionScope.USER.username}"/>
                                        <input type="submit" class="btn btn-outline-dark" name="action" value="Edit"/>
                                    </form>
                                </li>
                                <li>
                                    <form action="searchTourPage.jsp" method="POST">
                                        <input type="submit" class="btn btn-outline-dark" value="Tour Search"/>
                                    </form>
                                </li>
                                <li>            
                                    <form action="MainController" method="POST">
                                        <input type="submit" class="btn btn-outline-dark" name="action" value="Logout"/>
                                    </form>
                                </li>
                                <li>
                                    <form action="tourInsert.jsp" method="POST">
                                        <input type="hidden" name="USER" value="${sessionScope.USER}" />
                                        <input type="submit" class="btn btn-outline-dark" name="action" value="Insert new tour"/>
                                    </form>
                                </li>
                                <li>
                                    <form action="searchTourPage.jsp" method="POST">
                                        <input type="submit" class="btn btn-outline-dark" value="Tour Search"/>
                                    </form>
                                </li>
                                <!-- <li><a href="booking.html">Book Online</a></li> -->
                            </ul>
                        </nav>
                    </div>

                    <div class="col-6 col-xl-2 text-right">
                        <div class="d-none d-xl-inline-block">
                            <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                                <li>
                                    <a href="#" class="pl-0 pr-3 text-black"><span class="icon-tripadvisor"></span></a>
                                </li>
                                <li>
                                    <a href="#" class="pl-3 pr-3 text-black"><span class="icon-twitter"></span></a>
                                </li>
                                <li>
                                    <a href="#" class="pl-3 pr-3 text-black"><span class="icon-facebook"></span></a>
                                </li>
                                <li>
                                    <a href="#" class="pl-3 pr-3 text-black"><span class="icon-instagram"></span></a>
                                </li>

                            </ul>
                        </div>

                        <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a></div>

                    </div>

                </div>
            </div>
        </header>

        <section class="search-sec" style="margin-top: 10%">
            <div class="container">
                <form action="MainController" method="POST">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                                    <input type="text" placeholder="Username" name ="txtSearch"/>
                                    <font color="red">${requestScope.INVALID.usernameError}</font>
                                </div>
                                <br/>
                                <div class="col-lg-3 col-md-3 col-sm-12 p-0">
                                    <input type="hidden" name="USER" value="${sessionScope.USER}"/>
                                    <input type="submit" value="Search" name="action"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <br/>
            </div>
        </section>
        <c:if test="${requestScope.INFO != null}">
            <c:if test="${not empty requestScope.INFO}" var="checkData">
                 <table class="table table-hover shopping-cart-wrap" >
                        <thead class="text-muted" >
                        <tr>
                           <th scope="col" >No</th>
                            <th scope="col" >Username</th>
                            <th scope="col" >Fullname</th>
                            <th scope="col" >Role</th>
                            <th scope="col" >Delete</th>
                           <th scope="col" >Edit</th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="dto" items="${requestScope.INFO}" varStatus="counter">
                            <tr>
                                <td>${counter.count}</td>
                                <td>${dto.username}</td>
                                <td>${dto.fullname}</td>
                                <td>${dto.role}</td>
                                <td><c:url var="DeleteLink" value="MainController">
                                        <c:param name="action" value="Delete"/>
                                        <c:param name="username" value="${dto.username}"/>
                                        <c:param name ="txtSearch" value="${param.txtSearch}"/>
                                    </c:url>
                                    <button class="btn btn-danger"><a style="color: white" href="${DeleteLink}">Delete</a></button>
                                </td>
                                <td>
                                    <form action="MainController" method="POST">
                                        <input type="hidden" name="username" value="${dto.username}"/>
                                        <input type="hidden" name="txtSearch" value="${param.txtSearch}"/>
                                        <input type="submit" class="btn btn-danger" name="action" value="Edit"/>
                                    </form>
                                </td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>
            </c:if>
            <c:if test="${!checkData}">
                No record found
            </c:if>
        </c:if>
                <footer class="site-footer">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="mb-5">
                                    <h3 class="footer-heading mb-4">About Travelers</h3>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe pariatur reprehenderit vero atque, consequatur id ratione, et non dignissimos culpa? Ut veritatis, quos illum totam quis blanditiis, minima minus odio!</p>
                                </div>



                            </div>
                            <div class="col-lg-4 mb-5 mb-lg-0">
                                <div class="row mb-5">
                                    <div class="col-md-12">
                                        <h3 class="footer-heading mb-4">Navigations</h3>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <ul class="list-unstyled">
                                            <li><a href="#">Home</a></li>
                                            <li><a href="#">Destination</a></li>
                                            <li><a href="#">Services</a></li>
                                            <li><a href="#">About</a></li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-lg-6">
                                        <ul class="list-unstyled">
                                            <li><a href="#">About Us</a></li>
                                            <li><a href="#">Privacy Policy</a></li>
                                            <li><a href="#">Contact Us</a></li>
                                            <li><a href="#">Discounts</a></li>
                                        </ul>
                                    </div>
                                </div>



                            </div>

                            <div class="col-lg-4 mb-5 mb-lg-0">


                                <div class="mb-5">
                                    <h3 class="footer-heading mb-2">Subscribe Newsletter</h3>
                                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit minima minus odio.</p>

                                    <form action="#" method="post">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control border-secondary text-white bg-transparent" placeholder="Enter Email" aria-label="Enter Email" aria-describedby="button-addon2">
                                            <div class="input-group-append">
                                                <button class="btn btn-primary text-white" type="button" id="button-addon2">Send</button>
                                            </div>
                                        </div>
                                    </form>

                                </div>

                            </div>

                        </div>
                        <div class="row pt-5 mt-5 text-center">
                            <div class="col-md-12">
                                <div class="mb-5">
                                    <a href="#" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                                    <a href="#" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                                    <a href="#" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                                    <a href="#" class="pl-3 pr-3"><span class="icon-linkedin"></span></a>
                                </div>

                                <p>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                                    <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                </p>
                            </div>

                        </div>
                    </div>
                </footer>

                <script src="js/jquery-3.3.1.min.js"></script>
                <script src="js/jquery-migrate-3.0.1.min.js"></script>
                <script src="js/jquery-ui.js"></script>
                <script src="js/popper.min.js"></script>
                <script src="js/bootstrap.min.js"></script>
                <script src="js/owl.carousel.min.js"></script>
                <script src="js/jquery.stellar.min.js"></script>
                <script src="js/jquery.countdown.min.js"></script>
                <script src="js/jquery.magnific-popup.min.js"></script>
                <script src="js/bootstrap-datepicker.min.js"></script>
                <script src="js/aos.js"></script>

                <script src="js/main.js"></script>
    </body>
</html>
