<%-- 
    Document   : insert
    Created on : Feb 14, 2019, 1:35:05 PM
    Author     : mickw
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <!-- Font Icon -->
        <link rel="stylesheet" href="fonts/material-icon/css/material-design-iconic-font.min.css">

        <!-- Main css -->
        <link rel="stylesheet" href="CSS/styleRegister.css">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <div class="main">

            <section class="signup">
                <!-- <img src="images/signup-bg.jpg" alt=""> -->
                <div class="container">
                    <div class="signup-content">
                        <h2 class="form-title">Update Account</h2>
                        <form action="MainController" method="POST" id="signup-form" class="signup-form" >
                            <div class="form-group">
                                <input type="text" class="form-input" id="name" placeholder="Username"  value="${requestScope.DTO.username}" disabled="true"/>
                                <input type="hidden" name="txtUsername" value="${requestScope.DTO.username}"/>
                                <font color="red">
                                ${requestScope.INVALID.usernameError}
                                </font>
                            </div>

                            <div class="form-group">
                                <input type="text"  class="form-input" id="name" placeholder="Fullname" name="txtFullname" value="${requestScope.DTO.fullname}"/>
                                <font color="red">
                                ${requestScope.INVALID.fullnameError}
                                </font>
                            </div>

                            <div class="form-group">
                                <input type="text" class="form-input" name="txtEmail" id="email" placeholder="Your Email" value="${requestScope.DTO.email}"/>
                                <font color="red">
                                ${requestScope.INVALID.emailError}
                                </font>
                            </div>
                            <div class="form-group">
                                <input type="text" placeholder="PhoneNo" class="form-input" name="txtPhoneNo" value="${requestScope.DTO.phoneNo}"/>
                                <font color="red">
                                ${requestScope.INVALID.phoneNoError}
                                </font>
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="txtSearch" value="${param.txtSearch}"/>
                                <input type="hidden" name="txtRole" value="${requestScope.DTO.role}"/>
                                <input type="submit" name="action" id="submit" class="form-submit" value="Update"/>
                            </div>
                        </form>
                    </div>
                </div>
            </section>
        </div>
        <!-- JS -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="js/mainRegister.js"></script>
    </body>
</html>
