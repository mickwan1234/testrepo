USE [master]
GO
/****** Object:  Database [ProjectTour]    Script Date: 3/17/2019 10:14:19 PM ******/
CREATE DATABASE [ProjectTour]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'ProjectTour', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\ProjectTour.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'ProjectTour_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.SQLEXPRESS\MSSQL\DATA\ProjectTour_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [ProjectTour] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ProjectTour].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ProjectTour] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [ProjectTour] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [ProjectTour] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [ProjectTour] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [ProjectTour] SET ARITHABORT OFF 
GO
ALTER DATABASE [ProjectTour] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [ProjectTour] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [ProjectTour] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [ProjectTour] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [ProjectTour] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [ProjectTour] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [ProjectTour] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [ProjectTour] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [ProjectTour] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [ProjectTour] SET  DISABLE_BROKER 
GO
ALTER DATABASE [ProjectTour] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [ProjectTour] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [ProjectTour] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [ProjectTour] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [ProjectTour] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [ProjectTour] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [ProjectTour] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [ProjectTour] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [ProjectTour] SET  MULTI_USER 
GO
ALTER DATABASE [ProjectTour] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [ProjectTour] SET DB_CHAINING OFF 
GO
ALTER DATABASE [ProjectTour] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [ProjectTour] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [ProjectTour] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [ProjectTour] SET QUERY_STORE = OFF
GO
USE [ProjectTour]
GO
/****** Object:  Table [dbo].[Bill]    Script Date: 3/17/2019 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bill](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](50) NOT NULL,
	[TourID] [varchar](50) NOT NULL,
	[Quantity] [int] NOT NULL,
	[TotalPrice] [float] NOT NULL,
	[Date] [date] NOT NULL,
 CONSTRAINT [PK_Bill_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Region]    Script Date: 3/17/2019 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Region](
	[RegionID] [varchar](50) NOT NULL,
	[RegionName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY CLUSTERED 
(
	[RegionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Registration]    Script Date: 3/17/2019 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Registration](
	[Username] [varchar](50) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[Fullname] [varchar](50) NOT NULL,
	[PhoneNo] [varchar](15) NOT NULL,
	[Email] [varchar](100) NOT NULL,
	[Role] [varchar](50) NOT NULL,
	[isDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Registration] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tour]    Script Date: 3/17/2019 10:14:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tour](
	[ID] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Time] [date] NOT NULL,
	[Description] [varchar](1000) NULL,
	[EndDate] [date] NOT NULL,
	[Image] [varchar](500) NULL,
	[Price] [float] NOT NULL,
	[RegionID] [varchar](50) NOT NULL,
	[IsDelete] [bit] NOT NULL,
 CONSTRAINT [PK_Tour] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Bill] ON 

INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1, N'Nghia', N'Tour5', 1, 70000, CAST(N'2018-12-30' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (2, N'User1', N'Tour2', 1, 1000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (3, N'User1', N'Tour1', 1, 20000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (4, N'User1', N'Tour7', 1, 10000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (5, N'User1', N'Tour5', 2, 140000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1002, N'User1', N'Tour11', 1, 1000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1003, N'User1', N'Tour12', 1, 10000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1004, N'User1', N'Tour11', 2, 2000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1005, N'User1', N'Tour12', 1, 10000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1006, N'User1', N'Tour3', 1, 80000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1007, N'User1', N'Tour7', 1, 10000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1008, N'User1', N'Tour4', 1, 10000, CAST(N'2019-03-10' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1009, N'User', N'Tour5', 1, 80000, CAST(N'2019-03-14' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1010, N'User', N'Tour5', 4, 320000, CAST(N'2019-03-14' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1011, N'User1', N'Tour16', 1, 80000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1012, N'User1', N'Tour10', 12, 2160000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1013, N'User1', N'Tour16', 1, 80000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1014, N'User1', N'Tour10', 12, 2160000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1015, N'User', N'Tour11', 1, 100000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1016, N'User', N'Tour10', 1, 200000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1017, N'User', N'Tour11', 1, 100000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1018, N'User', N'Tour10', 1, 200000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1019, N'User', N'Tour11', 1, 100000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1020, N'User', N'Tour12', 3, 120000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1021, N'User', N'Tour10', 1, 200000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1022, N'User', N'Tour11', 1, 100000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1023, N'User', N'Tour12', 3, 120000, CAST(N'2019-03-17' AS Date))
INSERT [dbo].[Bill] ([ID], [Username], [TourID], [Quantity], [TotalPrice], [Date]) VALUES (1024, N'User', N'Tour10', 1, 200000, CAST(N'2019-03-17' AS Date))
SET IDENTITY_INSERT [dbo].[Bill] OFF
INSERT [dbo].[Region] ([RegionID], [RegionName]) VALUES (N'Af', N'Africa')
INSERT [dbo].[Region] ([RegionID], [RegionName]) VALUES (N'As', N'Asia')
INSERT [dbo].[Region] ([RegionID], [RegionName]) VALUES (N'Aus', N'Australia')
INSERT [dbo].[Region] ([RegionID], [RegionName]) VALUES (N'Eu', N'Europe')
INSERT [dbo].[Region] ([RegionID], [RegionName]) VALUES (N'SA', N'SouthAmerica')
INSERT [dbo].[Registration] ([Username], [Password], [Fullname], [PhoneNo], [Email], [Role], [isDelete]) VALUES (N'admin', N'admin', N'Nghia Nguyen Hieu', N'123456789', N'1234@123.com', N'Admin', 0)
INSERT [dbo].[Registration] ([Username], [Password], [Fullname], [PhoneNo], [Email], [Role], [isDelete]) VALUES (N'Nghia', N'123', N'Nghia', N'1234567891', N'123@213.com', N'User', 0)
INSERT [dbo].[Registration] ([Username], [Password], [Fullname], [PhoneNo], [Email], [Role], [isDelete]) VALUES (N'User', N'user', N'Nghia', N'1234567891', N'123@213.com', N'User', 0)
INSERT [dbo].[Registration] ([Username], [Password], [Fullname], [PhoneNo], [Email], [Role], [isDelete]) VALUES (N'User1', N'user', N'Nghia', N'1234567891', N'123@213.com', N'User', 0)
INSERT [dbo].[Registration] ([Username], [Password], [Fullname], [PhoneNo], [Email], [Role], [isDelete]) VALUES (N'user2', N'user', N'Nghia', N'1234567891', N'123@213.com', N'User', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour1', N'London', CAST(N'2019-12-10' AS Date), N'See the English capital’s top landmarks on this most comprehensive of London magical tours. Take a tour of beautiful St Paul’s Cathedral, then make a photo stop at Westminster Abbey. Watch the Changing of the Guard at Buckingham Palace and visit the Tower of London, where you''ll have free time for lunch (own expense). Enjoy an extended river sightseeing cruise, and upgrade to cap everything with a London Eye flight or cream tea at Harrods.

Read more about Tower of London, Changing of the Guard Tour and Thames Cruise 2019', CAST(N'2020-12-10' AS Date), N'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e9/Super_moon_over_City_of_London_from_Tate_Modern_2018-01-31_4.jpg/1000px-Super_moon_over_City_of_London_from_Tate_Modern_2018-01-31_4.jpg', 80000, N'Eu', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour10', N'Hanoi', CAST(N'2019-12-06' AS Date), N'Experience majestic Halong Bay in one day, with round-trip transport from Hanoi via the Red River Delta. Aboard a traditional Chinese junk, you''ll cruise past an epic landscape of limestone pillars and tiny islets. Enjoy a fresh seafood lunch, take a kayak or bamboo boat ride, and visit two stunning caves: Thien Cung and Dau Go. Upgrade for a private tour with your own personal guide and transport vehicle.
', CAST(N'2019-12-13' AS Date), N'http://static.asiawebdirect.com/m/bangkok/portals/vietnam/homepage/hanoi/pagePropertiesOgImage/teaser_006.jpg.jpg', 200000, N'As', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour11', N'Malaysia', CAST(N'2019-12-10' AS Date), N'Trade city streets for cave shrines, wildlife, and a spectacular firefly display on a full-day, small-group trip from Kuala Lumpur that explores some of the regionÃ¢Â?Â?s top sites. YouÃ¢Â?Â?ll climb into the maze of caverns at Batu Caves, spot silvered-leaf monkeys and macaques at Melawati Hill, pause for a seafood dinner, then cruise a dark river lit by sparkling fireflies. Group size is limited to eight people to ensure personal service from the guides, with dinner and all transport included.

Read more about Batu Caves, Kuala Selangor Firefly Cruise from Kuala Lumpur 2019', CAST(N'2020-12-14' AS Date), N'https://znews-photo.zadn.vn/w660/Uploaded/wyhktpu/2018_02_13/1.jpg', 100000, N'As ', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour12', N'Mexico', CAST(N'2019-12-10' AS Date), N'Avoid the crowds on this early morning tour of the Teotihuacan pyramids, also known as the City of the Gods. On this tour, an archeologist takes you around the UNESCO World Heritage site and gives you insight into the majestic plazas and murals. Snap memorable shots from the top of Sun Pyramid and Moon Pyramid, and then cap off your day trip from Mexico City with a visit to an obsidian factory.

Read more about Early Morning Teotihuacan Pyramids Tour with a Private Archeologist 2019 - Mexico City', CAST(N'2020-12-10' AS Date), N'http://52.24.98.51/wp-content/uploads/2018/02/mexico-city.jpg', 40000, N'SA ', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour13', N'Seoul', CAST(N'2019-12-10' AS Date), N'The Demilitarized Zone (DMZ) lies on the border between North and South Korea, and visiting outside of a guided tour is not allowed. On this tour, explore the DMZ with your guide and gain insight into Korean War history. Take in sites of interest including the 3rd Tunnel and the Dora Observatory in safety, with entrance fees plus pickup and drop-off in central Seoul included.

Read more about Half-Day Korean DMZ Tour from Seoul 2019', CAST(N'2020-12-10' AS Date), N'https://www.dulichvietnam.com.vn/kinh-nghiem/wp-content/uploads/2018/12/nhung-noi-nen-den-o-seoul-canh-dep-khien-ban-say-me-quen-loi-ve.gif', 120000, N'As', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour16', N'Tokyo', CAST(N'2019-12-10' AS Date), N'Join a full-day guided tour from Tokyo that travels to Mt Fuji, then continues to nearby Lake Ashi and the Mt Komagatake Ropeway. Enjoy the ease of comfortable transportation and a knowledgeable guide, who will accompany you to the revered Mt Fuji''s 5th Station. Take a short boat cruise on Lake Ashi and climb by aerial tram to the top of Mt Komagatake for views of Mt Fuji. Return to Tokyo by bullet train in the evening after taking in some of Japan’s iconic highlights.

Read more about Mt. Fuji, Lake Ashi and Bullet Train Day Tour from Tokyo 2019', CAST(N'2020-12-10' AS Date), N'https://travel.com.vn/images/destination/Large/dc_171214_tokyo-japan-FDEALS0217.jpg', 80000, N'As ', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour18', N'Singapore', CAST(N'2019-12-10' AS Date), N'Marvel at the illuminated sights of Singapore during a 3.5-hour night tour. Explore top attractions with a knowledgeable guide, including a synchronized light and music show at the Supertree Grove at Gardens by the Bay. Visit the Sands Skypark Observation Deck and then finish with a 20-minute cruise on the Singapore River.

Read more about Singapore Night Tour with Gardens by the Bay and River Cruise 2019', CAST(N'2020-12-10' AS Date), N'https://vnn-imgs-f.vgcloud.vn/2018/05/11/09/vi-sao-singapore-duoc-chon-cho-hoi-nghi-trump-kim.jpg', 700000, N'As ', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour2', N'Spain', CAST(N'2019-12-10' AS Date), N'The long lines attracted by La Sagrada Familia and Park Guell, two of Antoni Gaudi’s modernist masterpieces, are testament to their global renown. Spend less time lining up and more time sightseeing with skip-the-line entry to both the cathedral and park on a 4-hour guided tour. Feast your eyes on the spectacular interior and exterior of La Sagrada Familia before proceeding to Park Guell by car.

Read more about Skip the Line: Park Guell and La Sagrada Familia Guided Tour 2019 - Barcelona', CAST(N'2020-12-10' AS Date), N'https://cdn.cnn.com/cnnnext/dam/assets/170706113411-spain.jpg', 800000, N'Eu', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour20', N'Wales', CAST(N'2020-10-18' AS Date), N'For a place with such exciting landscapes â?? from snow-capped peaks to wild coastline â?? Wales is compact and neighbourly. Come to switch off, and explore untouched valleys and sleepy villages. Or come to enjoy raring-to-go towns and the capital Cardiff â?? voted Britainâ??s most sociable city.

2019 is Walesâ?? Year of Discovery, inviting you to explore the country and find its bountiful treasures. Delve into Walesâ?? epic heritage with more than 600 castles to explore. Follow the North Wales Way taking in the magnificent Beaumaris, Caernarfon and Harlech castles â?? all World Heritage Sites. Trek along the breathtaking Cambrian Way winding through Snowdonia and the Brecon Beacons. Uncover the inspiration to Dylan Thomasâ?? soul-stirring poetry in Laugharne or feast your eyes on one of the best impressionist collections in Europe at the National Museum Wales.', CAST(N'2020-10-18' AS Date), N'https://www.visitbritain.com/sites/default/files/consumer_destinations/teaser_images/wales_teaser.jpg', 800000, N'Eu ', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour3', N'Berlin', CAST(N'2019-12-10' AS Date), N'Celebrate your visit to Berlin with a sky-high meal in the clouds during this panorama dinner 679 feet (207 meters) above the city at Sphere, the Berlin TV Tower''s revolving restaurant. Enjoy skip-the-line access to the tallest building in Berlin, where you''ll sit down to a panorama dinner. Savor a delicious 3-course dinner at a guaranteed window-seat table, accompanied by three wines, and soak up the incredible vistas of nearby Berlin landmarks such as Charlottenburg Palace and Checkpoint Charlie.

Read more about Berlin TV Tower Skip-the-Line Access with Dinner & Wine 2019 ', CAST(N'2020-12-10' AS Date), N'https://duhocduc.edu.vn/wp-content/uploads/2014/05/Berlin-Stadt.jpg', 400000, N'Eu', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour4', N'Beijing', CAST(N'2019-12-10' AS Date), N'Visit the Great Wall of China at Mutianyu and the Summer Palace (Yiheyuan), both UNESCO World Heritage sites, on a full-day private tour from Beijing. The Great Wall of China is one of the most popular attractions in China, yet the Mutianyu section attracts fewer visitors, which means you get a less crowded experience of this world-famous landmark. Then learn about the history of the majestic Summer Palace as you stroll through its lavish gardens, sprinkled with temples and other eye-catching features. Includes guide, lunch, entrance fees, Great Wall cable car ticket, private vehicle, and hotel pickup and drop-off.

Read more about Mutianyu Great Wall, Summer Palace Private Tour from Beijing 2019', CAST(N'2020-12-10' AS Date), N'https://www.visitberlin.de/system/files/styles/visitberlin_teaser_single_visitberlin_mobile_1x/private/image/_SCH6057_c_Scholvien_PSR_SC_STD_V2_DL_PPT_0.jpg?h=32462309&itok=Xi0CMgn5', 120000, N'As', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour5', N'Moscow', CAST(N'2019-12-10' AS Date), N'Spend three days taking in Moscow’s sights as you learn the history of the city, dating back to the 12th century. Walk through one of the oldest quarters of Moscow, the Kitay Gorod neighborhood. See the historical Red Square, Christ the Savior Cathedral, Patriarch''s Bridge, Gorky Park, and the Changing of the Guards Ceremony in Alexander Gardens. Stroll through the Downtown and Zamoskvorechye Districts to see 16th-19th century churches and cathedrals. Then journey to Patriarch''s Ponds residential area and Pushkin Square to see a more modern side of the city.

Read more about Moscow Ultimate 3-Day Tour 2019 ', CAST(N'2020-12-10' AS Date), N'https://cdni.rbth.com/rbthmedia/images/2018.02/original/5a95227d15e9f9034f79d275.jpg', 820000, N'As', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour7', N'Kyoto', CAST(N'2019-12-10' AS Date), N'This guided tour helps you discover lots of Kyoto’s dispersed temples, shrines, and cultural attractions in one day. You’ll save time researching and planning your trip, by having the itinerary organized for you. You’ll travel in an air-conditioned coach, and it’ll be a hassle-free day. Highlights include Kiyomizu-Dera Temple, Fushimi Inari Shrine, Sagano Bamboo Forest, and Tenryuji Temple.

Read more about Kyoto Cultural Forest, Shrine and Temple Tour with Options ', CAST(N'2020-12-10' AS Date), N'https://cdn.japantimes.2xx.jp/wp-content/uploads/2018/04/n-kyoto-a-20180406-870x580.jpg', 422000, N'As', 0)
INSERT [dbo].[Tour] ([ID], [Name], [Time], [Description], [EndDate], [Image], [Price], [RegionID], [IsDelete]) VALUES (N'Tour8', N'Bangkok', CAST(N'2019-12-10' AS Date), N'This 4-hour cultural night tour by tuk-tuk provides a great introduction to Bangkok for first-time visitors, but it also showcases a side of the city that most repeat visitors haven''t seen. You get to bypass heavily trafficked areas and take off-the-beaten-track routes toward illuminated temples such as Wat Pho and a bustling, 24-hour flower market. Enjoy dinner, followed by dessert at a secret, mystery stop.

Read more about Bangkok Food, Market, Temple Evening Tour by Tuk Tuk 2019', CAST(N'2019-12-10' AS Date), N'https://fantasea.vn/wp-content/uploads/2018/05/Th%C3%A1i-Lan.jpeg', 20000, N'As ', 0)
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Registration] FOREIGN KEY([Username])
REFERENCES [dbo].[Registration] ([Username])
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [FK_Bill_Registration]
GO
ALTER TABLE [dbo].[Bill]  WITH CHECK ADD  CONSTRAINT [FK_Bill_Tour] FOREIGN KEY([TourID])
REFERENCES [dbo].[Tour] ([ID])
GO
ALTER TABLE [dbo].[Bill] CHECK CONSTRAINT [FK_Bill_Tour]
GO
ALTER TABLE [dbo].[Tour]  WITH CHECK ADD  CONSTRAINT [FK_Tour_Region] FOREIGN KEY([RegionID])
REFERENCES [dbo].[Region] ([RegionID])
GO
ALTER TABLE [dbo].[Tour] CHECK CONSTRAINT [FK_Tour_Region]
GO
USE [master]
GO
ALTER DATABASE [ProjectTour] SET  READ_WRITE 
GO
