/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.models;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

/**
 *
 * @author mickw
 */
public class RegistrationDAO implements Serializable{

    public RegistrationDAO() {
    }
    public boolean insert(String username, String password, String fullname, String role) throws Exception{
        boolean check = false;
        String connStr = "jdbc:sqlserver://localhost:1433; databaseName = SinhVien; "
                + "user = sa; password = 1234";
        String sql = "Insert into Registration (Username, password, Fullname, Role) values (?,?,?,?)";
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        try(Connection conn = DriverManager.getConnection(connStr);
                PreparedStatement ps = conn.prepareStatement(sql)){
            ps.setString(1, username);
            ps.setString(2, password);
            ps.setString(3, fullname);
            ps.setString(4, role);
            check = ps.executeUpdate() > 0;
        }
        return check;
    }
}
