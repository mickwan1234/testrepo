<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri= "/WEB-INF/struts-logic.tld" prefix="logic" %>
<html>
    <body>
        <h1>Insert Account </h1>
        <logic:messagesPresent>
            <html:messages id="error">
                <font color="red">
                <ul>
                    <li>
                        ${error}
                    </li>
                </ul>
                </font>
            </html:messages>
        </logic:messagesPresent>
            <html:form action="/Insert" onsubmit="return validateinsertDynamicForm(this);">
            Username: <html:text property="username"/>
            <br/>
            Password: <html:password property="password"/>
            <br/>
            Email: <html:text property="emailID"/>
            <br/>
            PhoneNo (ex: 090-1234567): <html:text property="phoneNo"/>
            <br/>
            <html:submit value="Insert"/>
        </html:form>
    </body>
</html>