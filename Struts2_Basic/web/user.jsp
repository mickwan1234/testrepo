<%-- 
    Document   : user
    Created on : Mar 5, 2019, 1:40:54 PM
    Author     : mickw
--%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Welcome, <s:property value="%{#session.USER}"/></h1>
    </body>
</html>
