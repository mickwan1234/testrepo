<%-- 
    Document   : error
    Created on : Mar 5, 2019, 1:35:59 PM
    Author     : mickw
--%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Error Page</h1>
        <h2>
            <font color ="red">
            <s:property value="%{#request.ERROR}"/>
            </font>
        </h2>
        <a href="index.jsp">back to login page</a>
    </body>
</html>
