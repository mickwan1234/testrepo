/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.daos;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import nghia.connection.MyConnection;
import nghia.dtos.RegistrationDTO;

/**
 *
 * @author mickw
 */
public class RegistrationDAO implements Serializable {

    private Connection conn = null;
    private PreparedStatement ps = null;
    private ResultSet rs = null;

    private void closeConnection() throws Exception {
        if (conn != null) {
            conn.close();
        }
        if (ps != null) {
            ps.close();
        }
        if (rs != null) {
            rs.close();
        }
    }

    public String checkLogin(String username, String password) throws Exception {
        String role = "failed";
        try {
            String sql = "select Role from Registration where Username = ? and Password = ?";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, username);
            ps.setString(2, password);
            rs = ps.executeQuery();
            if (rs.next()) {
                role = rs.getString("Role");
            }
        } finally {
            closeConnection();
        }
        return role;
    }

    public List<RegistrationDTO> findByLikeName(String search) throws Exception {
        List<RegistrationDTO> dto = new ArrayList<>();
        String username = null;
        String fullname= null;
        String role = null;
        try {
            String sql = "Select * from Registration where fullname like ?";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, "%" + search + "%");
            rs = ps.executeQuery();
            while (rs.next()) {
                username = rs.getString("Username");
                role = rs.getString("Role");
                fullname = rs.getString("Fullname");
                RegistrationDTO dtos = new RegistrationDTO(username, role, fullname);
                dto.add(dtos);
            }
            System.out.println(dto);
        } finally {
            closeConnection();
        }
        return dto;
    }
    
    public boolean delete(String id)throws Exception{
        boolean check = false;
        try{
            String sql = "Delete from registration where Username = ?";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, id);
            check = ps.executeUpdate() > 0;
        }finally{
            closeConnection();
        }
        return check;
    }
    
    public boolean insert(RegistrationDTO dto) throws Exception{
        boolean check = false;
        try{
            String sql = "Insert into Registration(Username, Password, fullname, Role) values(?,?,?,?)";
            conn = MyConnection.myConnection();
            ps = conn.prepareStatement(sql);
            ps.setString(1, dto.getUsername());
            ps.setString(2, dto.getPassword());
            ps.setString(3, dto.getFullname());
            ps.setString(4, dto.getRole());
            check = ps.executeUpdate() > 0;
        }finally{
            closeConnection();
        }
        return check;
    } 
}
