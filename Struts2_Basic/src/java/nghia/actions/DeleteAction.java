/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.actions;

import nghia.daos.RegistrationDAO;

/**
 *
 * @author mickw
 */
public class DeleteAction {
    String id;
private static final String FAIL = "fail";
private static final String SUCCESS = "success";
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    
    
    public DeleteAction() {
    }
    
    public String execute() throws Exception {
        String url = FAIL;
        RegistrationDAO dao = new RegistrationDAO();
        if(dao.delete(id)){
            url= SUCCESS;
        }
        return url;
    }
    
}
