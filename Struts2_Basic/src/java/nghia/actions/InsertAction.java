/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import nghia.daos.RegistrationDAO;
import nghia.dtos.RegistrationDTO;

/**
 *
 * @author mickw
 */
public class InsertAction extends ActionSupport {

    private static final String ERROR = "error";
    private static final String SUCCESS = "success";

    private String username, password, phoneNo, confirm, emailID;

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public void setEmailID(String EmailID) {
        this.emailID = EmailID;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public String getEmailID() {
        return emailID;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public static String getNONE() {
        return NONE;
    }

    public static String getINPUT() {
        return INPUT;
    }

    public static String getLOGIN() {
        return LOGIN;
    }

    public static String getERROR() {
        return ERROR;
    }

    public static String getSUCCESS() {
        return SUCCESS;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirm() {
        return confirm;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setConfirm(String confirm) {
        this.confirm = confirm;
    }

    public InsertAction() {
    }

    public String execute() throws Exception {
        String url = ERROR;
        RegistrationDAO dao = new RegistrationDAO();
        RegistrationDTO dto = new RegistrationDTO(username, phoneNo, emailID);
        dto.setPassword(password);
        if (dao.insert(dto)) {
            url = SUCCESS;
        }
        return url;
    }

}
