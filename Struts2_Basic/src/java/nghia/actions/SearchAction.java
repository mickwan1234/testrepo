/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.actions;

import java.util.List;
import javax.servlet.Registration;
import nghia.daos.RegistrationDAO;
import nghia.dtos.RegistrationDTO;

/**
 *
 * @author mickw
 */
public class SearchAction {
    String searchValue;
    private List<RegistrationDTO> list;

    public List<RegistrationDTO> getList() {
        return list;
    }
    public String getSearchValue() {
        return searchValue;
    }

    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }
    
    
    public SearchAction() {
    }
    
    public String execute() throws Exception {
        RegistrationDAO dao = new RegistrationDAO();
        list = dao.findByLikeName(searchValue);
        return "success";
    }
    
}
