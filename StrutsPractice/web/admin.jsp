<%-- 
    Document   : admin
    Created on : Mar 11, 2019, 7:51:23 PM
    Author     : mickw
--%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <s:head/>
    </head>
    <body>
        <h1>Admin page <s:property value="%{#session.USER}"/></h1>
        <s:form action="SearchAction">
        <s:textfield name="searchValue" label="Search by fullname"/>
        <s:submit value="Search"/>
        </s:form>
        <s:if test="%{list != null}">
            <table border="1">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Username</th>
                        <th>Fullname</th>
                        <th>Role</th>
                        <th>Edit</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    <s:iterator value="list" status="counter">
                    <tr>
                        <td><s:property value="%{#counter.count}"/></td>
                        <td><s:property value="%{username}"/></td>
                        <td><s:property value="%{fullname}"/></td>
                        <td><s:property value="%{role}"/></td>
                        <td><s:form action="EditAction">
                                <s:hidden name="LastSearchValue" value="%{searchValue}"/>
                                <s:hidden value="%{username}" name="username"/>
                                <s:submit value="Edit"/>
                        </s:form></td>
                        <td><s:form action="DeleteAction">
                                <s:hidden name="LastSearchValue" value="%{searchValue}"/>
                                <s:hidden name="username" value="%{username}"/>
                                <s:submit value="Delete"/>
                        </s:form></td>
                    </tr>
                    </s:iterator>
                </tbody>
            </table>
        </s:if>
        <s:if test="%{list == null}">
            <font color="red">Nothing found</font>
        </s:if>
    </body>
</html>
