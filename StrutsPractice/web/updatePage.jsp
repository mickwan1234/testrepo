<%-- 
    Document   : UpdatePage
    Created on : Mar 12, 2019, 8:55:45 AM
    Author     : mickw
--%>
<%@taglib uri="/struts-tags" prefix="s" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <s:head/>
    </head>
    <body>
        <h1>Update Page</h1>
        <s:form action="UpdateAction">
            <s:textfield name="username" value="%{dto.username}" label="Username" readonly="true"/>
            <s:textfield name="fullname" value="%{dto.fullname}" label="Fullname" />
            <s:textfield name="role" value="%{dto.role}" label="Role"/>
            <s:submit value="Update"/>
        </s:form>
    </body>
</html>
