/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.actions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import nghia.connections.MyConnection;
import nghia.dtos.RegistrationDTO;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author mickw
 */
public class EditAction extends ActionSupport {
    private String username;
    private RegistrationDTO dto; 

    public RegistrationDTO getDto() {
        return dto;
    }

    public void setDto(RegistrationDTO dto) {
        this.dto = dto;
    }
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public static void setLOG(Logger LOG) {
        ActionSupport.LOG = LOG;
    }
    
    public EditAction() {
    }
    
    public String execute() throws Exception {

        String sql = "Select username, fullname, role from Registration where username = ?";
        Connection conn = MyConnection.getMyConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, username);
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            String username = rs.getString("Username");
            String fullname = rs.getString("Fullname");
            String role = rs.getString("Role");
            dto = new RegistrationDTO(username, fullname, role);
        }
        return "success";
    }
    
}
