/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.actions;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.http.HttpServletRequest;
import nghia.connections.MyConnection;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author mickw
 */
public class DeleteAction extends ActionSupport {

    private String username;
    
    private static final String ERROR = "error";
    private static final String SUCCESS = "success";

    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public static Logger getLOG() {
        return LOG;
    }
    
    public static void setLOG(Logger LOG) {
        ActionSupport.LOG = LOG;
    }
    
    public DeleteAction() {
    }
    
    public String execute() throws Exception {
        String url = ERROR;
        String sql = "Delete from Registration where username = ?";
        Connection conn = MyConnection.getMyConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        HttpServletRequest request = ServletActionContext.getRequest();
        ps.setString(1, username);
        boolean check = ps.executeUpdate() > 0;
        if(check){
            url = SUCCESS;
        }else{
            request.setAttribute("ERROR", "Delete failed");
        }
        return url;
    }
    
}
