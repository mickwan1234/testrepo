/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.actions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import nghia.connections.MyConnection;
import nghia.dtos.RegistrationDTO;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author mickw
 */
public class SearchAction extends ActionSupport {

    private String searchValue;
    private ArrayList<RegistrationDTO> list = null;

    public ArrayList<RegistrationDTO> getList() {
        return list;
    }

    public void setList(ArrayList<RegistrationDTO> list) {
        this.list = list;
    }

    public String getSearchValue() {
        return searchValue;
    }

    @Validations(
            requiredStrings = {
                @RequiredStringValidator(type = ValidatorType.FIELD, message = "Fullname is required")
            }
    )
    public void setSearchValue(String searchValue) {
        this.searchValue = searchValue;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public static void setLOG(Logger LOG) {
        ActionSupport.LOG = LOG;
    }

    public SearchAction() {
    }

    public String execute() throws Exception {
        Map session = ActionContext.getContext().getSession();
        HttpServletRequest request = ServletActionContext.getRequest();
        String username1 = (String) session.get("USER");
        if (username1 == null) {
            return "Error";
        }
        list = new ArrayList<>();
        String sql = "Select username,fullname,role from Registration where fullname like ?";
        Connection conn = MyConnection.getMyConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, "%" + searchValue + "%");
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String username = rs.getString("Username");
            String fullname = rs.getString("Fullname");
            String role = rs.getString("Role");
            RegistrationDTO dto = new RegistrationDTO(username, fullname, role);
            list.add(dto);
        }
        return "success";
    }

}
