/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.actions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.validator.annotations.EmailValidator;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.StringLengthFieldValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import nghia.connections.MyConnection;
import org.apache.struts2.ServletActionContext;

/**
 *
 * @author mickw
 */
public class UpdateAction extends ActionSupport {
    private String fullname, role;

    public String getFullname() {
        return fullname;
    }
@Validations(
requiredStrings = {
    @RequiredStringValidator(type = ValidatorType.FIELD, message = "fullname cannot be blank")
}
)
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getRole() {
        return role;
    }
@Validations(
requiredStrings = {
    @RequiredStringValidator(type = ValidatorType.FIELD, message = "Role cannot be blank")
},
        stringLengthFields = {
                @StringLengthFieldValidator(type = ValidatorType.FIELD, maxLength = "10", minLength = "2",
                        message = "Role must between 2 and 10 characters"
                        )
                
        }
)

    public void setRole(String role) {
        this.role = role;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public static void setLOG(Logger LOG) {
        ActionSupport.LOG = LOG;
    }
    
    
    public UpdateAction() {
    }
    
    public String execute() throws Exception {
    String url = "error";
        Map session = ActionContext.getContext().getSession();
        HttpServletRequest request = ServletActionContext.getRequest();
    String sql ="Update Registration set Fullname = ?, Role = ?";
        Connection conn = MyConnection.getMyConnection();
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, fullname);
        ps.setString(2, role);
        boolean check = ps.executeUpdate() > 0;
        if(check){
            url = "success";
        }else{
            request.setAttribute("ERROR", "Update failed");
        }
        return url;
    }
    
}
