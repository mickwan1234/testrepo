/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nghia.actions;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.util.logging.Logger;
import com.opensymphony.xwork2.validator.annotations.RequiredStringValidator;
import com.opensymphony.xwork2.validator.annotations.Validations;
import com.opensymphony.xwork2.validator.annotations.ValidatorType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import nghia.connections.MyConnection;
import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ExceptionMapping;
import org.apache.struts2.convention.annotation.Result;

/**
 *
 * @author mickw
 */
public class LoginAction extends ActionSupport {

    private static final String USER = "user";
    private static final String ADMIN = "admin";
    private static final String ERROR = "error";
    private String username, password;

    public String getUsername() {
        return username;
    }

    @Validations(
            requiredStrings = {
                @RequiredStringValidator(type = ValidatorType.FIELD, message = "username required")}
    )
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    @Validations(
            requiredStrings = {
                @RequiredStringValidator(type = ValidatorType.FIELD, message = "Password is required")
            }
    )
    public void setPassword(String password) {
        this.password = password;
    }

    public static Logger getLOG() {
        return LOG;
    }

    public static void setLOG(Logger LOG) {
        ActionSupport.LOG = LOG;
    }

    public LoginAction() {
    }

    public String execute() throws Exception {
                String url = ERROR;
        String role = "failed";
        String sql = "Select Role from Registration where username= ? and password = ?";
        HttpServletRequest request = ServletActionContext.getRequest();
        Connection conn = MyConnection.getMyConnection();
        PreparedStatement ps= conn.prepareStatement(sql);
        ps.setString(1, username);
        ps.setString(2, password);
        ResultSet rs = ps.executeQuery();
        if(rs.next()){
            role = rs.getString("Role");
        }
        Map session = ActionContext.getContext().getSession();
        session.put("USER", username);
        if(role.equalsIgnoreCase("User")){
            url = USER;
        }else if(role.equalsIgnoreCase("Admin")){
            url=ADMIN;
        }else if(role.equals("failed")){
            url = ERROR;
            request.setAttribute("ERROR", "Invalidname or password");
        }else{
            
        }
        return url;
    }

}
